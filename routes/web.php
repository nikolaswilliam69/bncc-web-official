<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Resources
Route::resource('/','MainController');
Route::resource('/product','ProductController');
Route::resource('/mission','MissionController');
Route::resource('/team','TeamController');
Route::resource('/feedback','FeedbackController');
Route::resource('/history','HistoryController');
Route::resource('/news','NewsController');
Route::resource('/event','EventController');
Route::resource('/about','AboutController');
Route::resource('/user','UserController');

Auth::routes();

Route::get('/','MainController@index');

if (Auth::check()) {
	return view('dashboard');
}

Route::get('/29adminPower', 'HomeController@index');

Route::get('/add-product',function(){
	return view('admins.ProductAdd');
});

Route::get('organization', 'MainController@org');
Route::get('news_all', 'MainController@news_all');
Route::get('show_news/{id}', 'MainController@show_news');

Route::get('/new', function () {
    return view('news');
});