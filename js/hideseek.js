$('.opt').css({left:'-100vh'}).hide();

function enter(input){
	console.log(input);
	$('#main').animate({
		right:'200vh'
	},800).fadeOut(function(){
		$('#'+input).fadeIn().animate({
			left:'0'
		});
		document.getElementById('level').scrollIntoView(true);
	});

	$('head').append('<link rel="stylesheet" href="assets/css/organization.css">');
};

function back(){
	$('.opt').animate({right:'200vh'},800).fadeOut(function(){
		$('#main').fadeIn().animate({
			right:'0'
		},1000);
		document.getElementById('Structure').scrollIntoView(true);
	});
}