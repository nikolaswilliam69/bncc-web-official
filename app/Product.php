<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
    	'product_name','product_logo','product_website','product_about','product_color',
    ];
}
