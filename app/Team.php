<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    public $timestamps = false;

    protected $fillable = [
    	'profile_photo','profile_name','profile_division','profile_sub_division','profile_position',
    ];
}
