<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
	public $timestamps = false;
    protected $fillable = [
    	'poster','title','date_start','date_end','time_start','time_end',
    ];
}
