<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// model
use App\Testimonial;

class
use Image;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimonials = Testimonial::all();
        return view('admins.TestiView');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admins.TestiAdd');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('photo')) {
            $name = $request->input('name');
            $photo = $request->file('photo');
            $filename = $name.'.'.$photo->getClientOriginalExtension();
            Image::make($photo)->save('assets/img/testimonials/'.$filename, 30);
        }

        Testimonial::create([
            'photo'=>$filename,
            'name'=>$request['name'],
            'position'=>$request['position'],
            'generation'=>$request['generation'],
        ]);

        return redirect('testi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $testi_te= Testimonial::findOrFail($id);
        $file='assets/img/testimonials/'.$testi_te->photo;
        unlink($file);

        if ($request->hasFile('photo')) {
            $name = $request->input('name');
            $photo = $request->file('photo');
            $filename = $name.'.'.$photo->getClientOriginalExtension();
            Image::make($photo)->save('assets/img/testimonials/'.$filename,30);
        }

        $testi = array(
            'photo'=>$filename,
            'name'=>$request['name'],
            'position'=>$request['position'],
            'generation'=>$request['generation'],
        );

        if($event == null){
            Event::create($testi);
        }
        else{
            Event::find($id)->update($testi);
        }

        return redirect('testi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $testi_te= Testimonial::findOrFail($id);
        $file='assets/img/testimonials/'.$testi_te->photo;
        unlink($file);

        $testi=Testimonial::where('id',$id);
        $testi->delete();

        return redirect('testi');
    }
}
