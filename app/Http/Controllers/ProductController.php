<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;

use Auth;
use Image;
use Closure;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware(function($request, Closure $next){
            if (Auth::user()->role == 'dpi' || Auth::user()->role == 'root') {
                return $next($request);
            }
            else{
                return redirect('29adminPower')->with('message', 'You Are Not Authorized to Access This Feature');
            }
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('admins.ProductView',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admins.ProductAdd');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('product_logo')) {
            $name = $request->input('product_name');
            $logo = $request->file('product_logo');
            $filename = 'logo'.'-'.$name.'.'.$logo->getClientOriginalExtension();
            Image::make($logo)->save('assets/img/products/'.$filename,30);
        }

        Product::create([
            'product_name'=>$request['product_name'],
            'product_logo'=>$filename,
            'product_website'=>$request['product_website'],
            'product_about'=>$request['product_about'],
            'product_color'=>$request['product_color'],
        ]);

        return redirect('29adminPower');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $product = Product::findOrFail($id);
        return view('admins.ProductEdit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product_temp= Product::findOrFail($id);
        $file='assets/img/products/'.$product_temp->product_logo;
        unlink($file);

        if ($request->hasFile('product_logo')) {
            $name=$request->input('product_name');
            $logo = $request->file('product_logo');
            $filename = 'logo'.'-'.$name.'.'.$logo->getClientOriginalExtension();
            Image::make($logo)->save('assets/img/products/'.$filename,30);
        }
        $product = array(
            'product_name'=>$request['product_name'],
            'product_logo'=>$filename,
            'product_website'=>$request['product_website'],
            'product_about'=>$request['product_about'],
            'product_color'=>$request['product_color'],
        );

        if($product == null){
            Product::create($product);
        }
        else{
            Product::find($id)->update($product);
        }

        return redirect('product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product_temp= Product::findOrFail($id);
        $file='assets/img/products/'.$product_temp->product_logo;
        unlink($file);

        $product=Product::where('id',$id);
        $product->delete();

        return redirect('product');
    }
}
