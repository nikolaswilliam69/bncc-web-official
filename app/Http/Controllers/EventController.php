<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Event;
use Image;
use Auth;

use Closure;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware(function($request, Closure $next){
            if (Auth::user()->role == 'eeo' || Auth::user()->role == 'root') {
                return $next($request);
            }
            else{
                return redirect('29adminPower')->with('message', 'You Are Not Authorized to Access This Feature');
            }
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::all();
        return view('admins.EventView', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admins.EventAdd');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('poster')) {
            $title = $request->input('title');
            $poster = $request->file('poster');
            $filename = $title.'.'.$poster->getClientOriginalExtension();
            Image::make($poster)->save('assets/img/events/'.$filename,30);
        }

        Event::create([
            'poster' => $filename,
            'title' => $request['title'],
            'date_start' => $request['date_start'],
            'date_end' => $request['date_end'],
            'time_start' => $request['time_start'],
            'time_end' => $request['time_end'],
        ]);
        return redirect('event');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::findOrFail($id);

        return view('admins.EventEdit',compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event_temp= Event::findOrFail($id);
        $file='assets/img/events/'.$event_temp->poster;
        unlink($file);

        if ($request->hasFile('poster')) {
            $title=$request->input('title');
            $poster = $request->file('poster');
            $filename = $title.'.'.$poster->getClientOriginalExtension();
            Image::make($poster)->save('assets/img/events/'.$filename,30);
        }
        $event = array(
            'poster' => $filename,
            'title' => $request['title'],
            'date_start' => $request['date_start'],
            'date_end' => $request['date_end'],
            'time_start' => $request['time_start'],
            'time_end' => $request['time_end'],
        );

        if($event == null){
            Event::create($event);
        }
        else{
            Event::find($id)->update($event);
        }

        return redirect('event');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event_temp= Event::findOrFail($id);
        $file='assets/img/events/'.$event_temp->poster;
        unlink($file);
        
        $event=Event::where('id',$id);
        $event->delete();

        return redirect('event');
    }
}
