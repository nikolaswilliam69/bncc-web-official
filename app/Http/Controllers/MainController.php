<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;
use App\History;
use App\Team;
use App\Event;
use App\News;
use App\About;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        $histories = History::orderBy('year')->get();
        $events = Event::all();
        $abouts = About::all();
        $today = date('Y-m-d');
        $news = News::all();
        return view('homepage-compro',compact('products','histories','events','today','news','abouts'));
    }

    public function Org()
    {
        $teams = Team::All();
        return view('organization',compact('teams'));
    }

    public function news_all()
    {
        $news = News::take(3);
        return view('news-all',compact('news'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_news($id)
    {
        $news = News::findOrFail($id);
        return view('news',compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
