<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Mission;

use Auth;
use Image;
use Closure;

class MissionController extends Controller
{
    public function __construct()
    {
        $this->middleware(function($request, Closure $next){
            if (Auth::user()->role == 'dpi' || Auth::user()->role == 'root') {
                return $next($request);
            }
            else{
                return redirect('29adminPower')->with('message', 'You Are Not Authorized to Access This Feature');
            }
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $missions = Mission::all();
        return view('admins.MissionView',compact('missions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admins.MissionAdd');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $test = Mission::count();
        if ($request->hasFile('mission_logo')) {
            $logo = $request->file('mission_logo');
            $filename = 'mission'.'-'.$test.'.'.$logo->getClientOriginalExtension();
            Image::make($logo)->save(public_path('assets/img/missions/'.$filename));
        }

        Mission::create([
            'mission_logo'=>$filename,
            'mission_detail'=>$request['mission_detail'],
        ]);

        return redirect('29adminPower');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mission = Mission::findOrFail($id);
        return view('admins.MissionEdit', compact('mission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
