<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Team;

use Auth;
use Image;
use Closure;

class TeamController extends Controller
{
    public function __construct()
    {
        $this->middleware(function($request, Closure $next){
            if (Auth::user()->role == 'dpi' || Auth::user()->role == 'root') {
                return $next($request);
            }
            else{
                return redirect('29adminPower')->with('message', 'You Are Not Authorized to Access This Feature');
            }
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Team::all();
        return view('admins.TeamView',compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admins.TeamAdd');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('profile_photo')) {
            $photo = $request->file('profile_photo');
            $name = $request->input('profile_name');
            $filename = 'profile'.'-'.$name.'.'.$photo->getClientOriginalExtension();
            Image::make($photo)->save('assets/img/teams/'.$filename,30);
        }

        Team::create([
            'profile_photo'=>$filename,
            'profile_name'=>$request['profile_name'],
            'profile_division'=>$request['profile_division'],
            'profile_sub_division'=>$request['profile_sub_division'],
            'profile_position'=>$request['profile_position'],
        ]);

        return redirect('team');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $team = Team::findOrFail($id);
        return view('admins.TeamEdit', compact('team'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $team_temp= Team::findOrFail($id);
        $file='assets/img/teams/'.$team_temp->profile_photo;
        unlink($file);
        
        if ($request->hasFile('profile_photo')) {
            $photo = $request->file('profile_photo');
            $name = $request->input('profile_name');
            $filename = 'profile'.'-'.$name.'.'.$photo->getClientOriginalExtension();
            Image::make($photo)->save('assets/img/teams/'.$filename,30);
        }

        $team = array(
            'profile_photo'=>$filename,
            'profile_name'=>$request['profile_name'],
            'profile_division'=>$request['profile_division'],
            'profile_sub_division'=>$request['profile_sub_division'],
            'profile_position'=>$request['profile_position'],
        );
        if($team == null){
            Team::create($team);
        }
        else{
            Team::find($id)->update($team);
        }

        return redirect('team');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team_temp= Team::findOrFail($id);
        $file='assets/img/teams/'.$team_temp->profile_photo;
        unlink($file);

        $team=Team::where('id',$id);
        $team->delete();

        return redirect('team');
    }
}
