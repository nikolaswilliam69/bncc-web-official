<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\News;

use Auth;
use Image;
use Closure;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware(function($request, Closure $next){
            if (Auth::user()->role == 'pr' || Auth::user()->role == 'root') {
                return $next($request);
            }
            else{
                return redirect('29adminPower')->with('message', 'You Are Not Authorized to Access This Feature');
            }
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::all();

        return view('admins.NewsView',compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admins.NewsAdd');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('image')) {
  			$title = $request->input('title');
            $image = $request->file('image');
            $filename = $title.'.'.$image->getClientOriginalExtension();
            Image::make($image)->save('assets/img/news/'.$filename,30);
        }
        News::create([
            'image'=>$filename,
            'title'=>$request['title'],
            'date'=>$request['date'],
            'content'=>$request['content'],
        ]);

        return redirect('news');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = News::findOrFail($id);
        return view('news',compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::findOrFail($id);

        return view('admins.NewsEdit',compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $news_temp= News::findOrFail($id);
        $file='assets/img/news/'.$news_temp->image;
        unlink($file);

        if ($request->hasFile('image')) {
            $title = $request->input('title');
            $image = $request->file('image');
            $filename = $title.'.'.$image->getClientOriginalExtension();    
            Image::make($image)->save('assets/img/news/'.$filename,30);
        }

        $news = array(
            'image'=>$filename,
            'title'=>$request['title'],
            'date'=>$request['date'],
            'content'=>$request['content'],
        );

        if($news == null){
            News::create($news);
        }

        else{
            News::find($id)->update($news);
        }

        return redirect('news');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news_temp= News::findOrFail($id);
        $file='assets/img/news/'.$news_temp->image;
        unlink($file);

        $news=News::where('id',$id);
        $news->delete();

        return redirect('news');
    }
}
