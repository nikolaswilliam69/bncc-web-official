<!doctype html>
<html lang="en">
    <head>
        <title>Bina Nusantara Computer Club - BNCC</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="icon" type="image/x-icon" href="{{asset('assets/img/favicon.ico')}}" />
        <!-- Framework CSS -->
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Raleway:300,400,500,800">
        <link rel="stylesheet" href="{{asset('assets/fontawesome/css/fontawesome-all.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
            <link rel="stylesheet" href="{{asset('assets/slick/slick.css')}}">
            <link rel="stylesheet" href="{{asset('assets/slick/slick-theme.css')}}"> 
        <link rel="stylesheet" href="{{asset('assets/css/animate.css')}}">        
        <!-- Custom CSS -->
        <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
        <link rel="stylesheet" href="{{asset('assets/animsition/css/animsition.min.css')}}">   
            <link rel="stylesheet" href="{{asset('assets/css/timeline.css')}}">
            <link rel="stylesheet" href="{{asset('assets/css/testimonial.css')}}">
        <!-- <link rel="stylesheet" href="assets/css/organization.css"> -->
    </head>
    <div class="js animsition">
        <body onload="pageReady();" data-spy="scroll">
            <div id="main">
                <!-- <div id="preloader"></div> -->
                <!-- Header Navigation -->
                <nav class="navbar fixed-top navbar-expand-lg navbar-dark navbar-no-bg">
                        <a class="navbar-brand" href="#Home">BNCC</a>             
                    <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#top-navbar-1" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="top-navbar-1">
                        <ul class="navbar-nav"> 
                        </ul>
                        <ul class="navbar-nav ml-auto navbar-light navbar-scrollable">
                            <li class="nav-item"><a class="nav-link" href="#Home">Home</a></li>
                            <li class="dropdown open"><a class="nav-link dropdown-toggle" href="#" id="aboutSectionDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">About Us</a>
                                <div class="dropdown-menu navbar-no-bg" aria-labelledby="aboutSectionDropdown">
                                    <a class="dropdown-item" href="#WhoAreWe">Who Are We ?</a>
                                    <a class="dropdown-item" href="#Vision">Vision</a>
                                    <a class="dropdown-item" href="#Mission">Mission</a>
                                </div>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="#Product">product</a></li>
                            {{-- <li class="nav-item"><a class="nav-link" href="#News">News</a></li> --}}
                            <li class="nav-item"><a class="nav-link" href="#Event">Event</a></li>
                            <li class="nav-item"><a class="nav-link" href="#Structure">Structure</a></li>
                            <li class="nav-item"><a class="nav-link" href="#History">History</a></li>
                            <!-- <li class="nav-item"><a class="nav-link" href="#Testimonial">Testimonial</a></li> -->
                            <!-- <li class="nav-item"><a class="nav-link" href="#">Contact Us</a></li> -->
                            <li class="nav-item"><a class="btn btn-link-3" href="#Contact">Contact</a></li>
                        </ul>
                    </div>
                </nav>
                
                <!-- Flexible Top Content (Image Transition Ready) -->
                <div class="top-content" id="Home">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 text wow fadeIn">
                                <!-- <h1>Bina Nusantara Computer Club</h1>
                                <div class="description">
                                    <p class="medium-paragraph">
                                        BNCC
                                    </p>
                                </div> -->
                                <img src="assets/img/BNCC_T.png" class="img-responsive" alt="" style="margin: auto;">
                            <div class="gojek-add">				
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gojek-logo" style="margin: auto;display:block;">
                                        <img src="assets/img/Gojek.png" class="img-responsive logo-gojek" alt="" style="margin: auto; margin-top:20px; max-width:300px;">
                                    </div>
                                </div>
                                <h3 style="color:white;margin-top:0;">Official Learning Partner</h2>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                    
                <!-- Who Are We Section -->
                <div class="section-container" >
                    <div class="container">
                        <div class="row" id="WhoAreWe">
                            <div class="col-sm-12 features section-description wow fadeInRight" >
                                <h2>Who Are <span> We ?</span></h2>
                                <div class="divider-1"><div class="line"></div></div>
                            </div>
                        </div>
                        
                        <div class="row wow fadeInLeftBig">
                            <div class="d-none d-md-block col-md-4" style="margin: auto;">
                                <img src="assets/img/BNCC_M_Original.png" alt="BNCC Logo Section">
                            </div>
                            <div class="col-12 col-md-8">
                                <div style="text-align: justify">
                                    <p>
                                        @foreach($abouts as $about)
                                            @if($about->type == 'Who Are We')
                                            {!!$about->content!!}
                                            @endif
                                        @endforeach
                                    </p>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="container-flex jumbtron paral parallax-4 darken">
                        <div class="row" id="Vision">
                            <div class="col-sm-12 features section-description wow fadeInRight" >
                                <h2>Our <span>Vision</span></h2>
                                <div class="divider-1"><div class="line"></div></div>
                            </div>
                        </div>
                            
                        <div class="row wow zoomIn">
                            <div class="col-1"></div>
                            <div class="col-10 text">
                                <h3>
                                    @foreach($abouts as $about)
                                        @if($about->type == 'Vission')
                                        {!!$about->content!!}
                                        @endif
                                    @endforeach
                                </h3>
                            </div>
                            <div class="col-1"></div>
                        </div>
                    </div>
                    <div class="container">
                            <div class="row" id="Mission">
                                    <div class="col-sm-12 features section-description wow fadeInRight" >
                                        <h2>Our <span>Mission</span></h2>
                                        <div class="divider-1"><div class="line"></div></div>
                                    </div>
                                </div>
                                    
                            <div class="row wow zoomIn">
                                <div class="container-hexa">
                                    <div class="row justify-content-center">
                                        <div class="col-12 col-md-12" >
                                            <div class="hexa text-center" id="hexagon"></div>
                                            <div class="hex-over"><img src="assets/svg/mission1.svg" class="m-1" onmouseover="change1()"></div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="col-12 col-md-3">
                                            <div class="row align-items-center h-50 justify-content-end">
                                                <div class="col-sm-12 col-md-6" >
                                                    <div class="hexa text-center" id="hexagon"></div>
                                                    <div class="hex-over"><img src="assets/svg/mission2.svg" class="m-1" onmouseover="change2()"></div>
                                                </div>
                                            </div>
                                            <div class="row align-items-center h-50 justify-content-end">
                                                <div class="col-sm-12 col-md-6" >
                                                    <div class="hexa text-center" id="hexagon"></div>
                                                    <div class="hex-over"><img src="assets/svg/mission3.svg" class="m-1" onmouseover="change3()"></div>
                                                </div>
                                            </div>
                                        </div>
                            
                                        <div class="col-12 col-md-5 mx-4 align-self-center" >
                                            <img src="assets/svg/mouse-hover.svg" id="iconDetail" >
                                            <h4 id="missionTextDetail">Hover the icon to show the detail</h4>
                                        </div>
                                
                                        <div class="col-12 col-md-3">
                                            <div class="row align-items-center h-50">
                                                <div class="col-12 col-md-6" >
                                                    <div class="hexa text-center" id="hexagon"></div>
                                                    <div class="hex-over"><img src="assets/svg/mission4.svg" class="m-1" onmouseover="change4()"></div>
                                                </div>
                                            </div>
                                            <div class="row align-items-center h-50">
                                                <div class="col-12 col-md-6" >
                                                    <div class="hexa text-center" id="hexagon"></div>
                                                    <div class="hex-over"><img src="assets/svg/mission5.svg" class="m-1" onmouseover="change5()"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="col-4 col-md-12" >
                                            <div class="hexa text-center" id="hexagon"></div>
                                            <div class="hex-over"><img src="assets/svg/mission6.svg" class="m-1" onmouseover="change6()"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                
                <!-- Product Section -->
                <div class="section-container">
                    <div class="container-flex jumbtron paral parallax-6 darken">
                        <div class="row" id="Product">
                            <div class="col-sm-12 features section-description wow fadeInRight" >
                                <h2>Our <span>Product</span></h2>
                                <div class="divider-1"><div class="line"></div></div>
                            </div>
                        </div>
                            
                        <div class="row wow flipInX">
                            @foreach($products as $product)
                                <div class="col-12 col-sm-4" style="margin: auto;">
                                    <div class="our-product">  
                                            <div class="button-div">
                                                    <a href="#fave" productid="{{$product->id}}" role="button" class="btn btn-outline-light btn-md button-product">About {{$product->product_name}}</a>
                                            </div>  
                                        <img class="our-product-image" src="{{asset('assets/img/products/'.$product->product_logo)}}" alt="Logo Fave">
                                    </div>
                                </div>
                            @endforeach

                            <div class="col-1"></div>

                            <div class="col-10">
                                <!-- p-5s p-sm-5 px-md-5 py-md-3 p-lg-5 -->
                                @foreach($products as $product)
                                    <div class="product-desc py-5"  productid="{{$product->id}}">
                                        <div id="product-desc-div">
                                            <h4><span style="color: {{$product->product_color}}">{{$product->product_name}}</span> {{$product->product_about}}</h4>
                                        </div>
                                        <div id="product-desc-url">
                                            <a class="h4 btn btn-outline-light" target="_blank" role="button" href="{{$product->product_website}}"><i class="fas fa-globe"></i> Visit {{$product->product_name}}</a>
                                        </div>                     
                                    </div>
                                @endforeach
                            </div>
                            <div class="col-1"></div>
                        </div>
                    </div>
                </div>

                <!-- News -->
		

                <!--Event Section -->
                <div class="section">
                    <div class="container-flex jumbtron paral parallax-2">
                        <div class="row p-3" id="Event">
                            <div class="col-12 features section-description wow fadeIn" >
                                <h2>Event</h2>
                                <div class="divider-1"><div class="line black"></div></div>
                            </div>

                            <div class="col-12 col-md-6 py-3 wow fadeIn">
                                <h3 class="carousel-event-title">Past Event</h3>     
                                <div id="carouselPastEvent" class="carousel slide" data-ride="carousel">
                                    <ul class="carousel-indicators">
                                        @php
                                            $i=0;
                                        @endphp
                                        @foreach($events as $event)
                                            @if($event->date_start < $today)
                                                @if($i == 0)
                                                    <li data-target="#carouselPastEvent" data-slide-to="{{$event->id}}" class="active"></li>
                                                @else
                                                    <li data-target="#carouselPastEvent" data-slide-to="{{$event->id}}"></li>
                                                @endif

                                                @php
                                                    $i++;
                                                @endphp
                                            @endif
                                        @endforeach
                                    </ul>
                                    <div class="carousel-inner">
                                        @php
                                            $i=0;
                                        @endphp
                                        @foreach($events as $event)
                                            @if($event->date_start < $today)
                                                @if($i == 0)
                                                    <div class="carousel-item active">
                                                @else
                                                    <div class="carousel-item">
                                                @endif
                                                        <div class="img-layer">
                                                            <img src="assets/img/events/{{$event->poster}}" alt="{{$event->title}}">
                                                        </div>
                                                        <div class="carousel-caption py-4">
                                                            <h3>{{$event->title}}</h3>
                                                            <div class="d-inline">
                                                                <div class="carousel-caption-box bg-success">
                                                                    <p><i class="fas fa-calendar-alt"></i> 
                                                                        @if($event->date_end == NULL)
                                                                            {{date('d F Y', strtotime($event->date_start))}}
                                                                        @else
                                                                            {{date('d', strtotime($event->date_start))}} - {{date('d F Y', strtotime($event->date_end))}}
                                                                        @endif
                                                                    </p>
                                                                </div>
                                                                <div class="carousel-caption-box">
                                                                    <p><i class="fas fa-clock"></i> 
                                                                        {{date('H:i',strtotime($event->time_start))}} - {{date('H:i',strtotime($event->time_end))}}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>   
                                                    </div>
                                            @php
                                                $i++;
                                            @endphp
                                            @endif
                                        @endforeach
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselPastEvent" data-slide="prev">
                                        <span class="carousel-control-prev-icon"></span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselPastEvent" data-slide="next">
                                        <span class="carousel-control-next-icon"></span>
                                    </a>
                                </div>
                            </div>

                            <div class="col-12 col-md-6 py-3 wow fadeIn">
                                <h3 class="carousel-event-title">Upcoming Event</h3>

                                <div id="carouselUpcomingEvent" class="carousel slide" data-ride="carousel">
                                    <ul class="carousel-indicators">
                                        @php
                                            $i=0;
                                        @endphp
                                        @foreach($events as $event)
                                            @if($event->date_start > $today)
                                                @php
                                                    $i++;
                                                @endphp
                                                @if($i == 1)
                                                    <li data-target="#carouselUpcomingEvent" data-slide-to="{{$event->id}}" class="active"></li>
                                                @else
                                                    <li data-target="#carouselUpcomingEvent" data-slide-to="{{$event->id}}"></li>
                                                @endif

                                                @if($i==0)
                                                    <li data-target="#carouselUpcomingEvent" data-slide-to="0" class="active"></li>
                                                @endif
                                                
                                            @endif
                                        @endforeach
                                    </ul>

                                    <div class="carousel-inner">
                                        @php
                                            $i=0;
                                        @endphp
                                        
                                        @foreach($events as $event)
                                            @if($event->date_start > $today)
                                                @php
                                                    $i++;
                                                @endphp
                                                @if($i == 1)
                                                    <div class="carousel-item active">
                                                @else
                                                    <div class="carousel-item">
                                                @endif
                                                        <div class="img-layer">
                                                            <img src="assets/img/events/{{$event->poster}}" alt="{{$event->title}}">
                                                        </div>
                                                        <div class="carousel-caption py-4">
                                                            <h3>{{$event->title}}</h3>
                                                            <div class="d-inline">
                                                                <div class="carousel-caption-box bg-danger">
                                                                    @if($event->date_end == NULL)
                                                                        <p><i class="fas fa-calendar-alt"></i> 
                                                                            {{date('d F Y',strtotime($event->date_start))}}
                                                                        </p>
                                                                    @else
                                                                        <p><i class="fas fa-calendar-alt"></i> 
                                                                            {{date('d',strtotime($event->date_start))}} - {{date('d F Y',strtotime($event->date_end))}}
                                                                        </p>
                                                                    @endif
                                                                </div>
                                                                <div class="carousel-caption-box">
                                                                    <p><i class="fas fa-clock"></i> 
                                                                        {{date('H:i',strtotime($event->time_start))}} - {{date('H:i',strtotime($event->time_end))}}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            @endif
                                        @endforeach
                                        @if($i == 0)
                                            <div class="carousel-item active">
                                                <div class="img-layer">
                                                    <!--<img src="assets/img/poster/default.jpg" alt="Coming Soon">-->
                                                </div>
                                                <div class="carousel-caption py-4">
                                                    <h3>Coming soon, stay tune !</h3>
                                                    <div class="d-inline">
                                                        <div class="carousel-caption-box bg-danger">
                                                            <p><i class="fas fa-calendar-alt"></i> Coming Soon</p>
                                                        </div>
                                                        <div class="carousel-caption-box">
                                                            <p><i class="fas fa-clock"></i> Coming Soon</p>
                                                        </div>
                                                    </div>
                                                </div>   
                                            </div>
                                        @endif
                                    </div>

                                    <a class="carousel-control-prev" href="#carouselUpcomingEvent" data-slide="prev">
                                        <span class="carousel-control-prev-icon"></span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselUpcomingEvent" data-slide="next">
                                        <span class="carousel-control-next-icon"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Organization Structure Section -->
                <div class="section">
                    <div class="container">
                        <div class="row" id="Structure"> 
                            <div class="col-sm-12 pb-5 section-description wow fadeInRight">
                                <a href="organization">
                                    <h2>Our <span>Organization</span> <i class="fas fa-caret-right"></i></h2>
                                    <h5>Click to show</h5>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- History Section -->
                <div class="section">
                    <div class="container">
                        <div class="row" id="History">
                            <div class="col-sm-12 section-description wow fadeInRight" >
                                <h2>Our <span>History</span></h2>
                                <div class="divider-1"><div class="line"></div></div>
                            </div>
                        </div>
                        <div class="row wow fadeInUp">
                            <div class="col-12">
                                <section class="cd-horizontal-timeline">
                                    <div class="timeline">
                                        <div class="events-wrapper">
                                            <div class="events">
                                                <ol>
                                                    @php
                                                        $i=0;
                                                    @endphp
                                                    @foreach($histories as $history)
                                                        <li>
                                                            @if($i==0)
                                                                <a href="#0" data-date="01/01/{{$history->year}}" class="selected">
                                                            @else
                                                                <a href="#0" data-date="01/01/{{$history->year}}">
                                                            @endif
                                                                    {{$history->year}}
                                                                </a>
                                                        </li>
                                                        @php
                                                            $i++;
                                                        @endphp
                                                    @endforeach
                                                </ol>
                                
                                                <span class="filling-line" aria-hidden="true"></span>
                                            </div> <!-- .events -->
                                        </div> <!-- .events-wrapper -->
                                            
                                        <ul class="cd-timeline-navigation">
                                            <li><a href="#0" class="prev inactive">Prev</a></li>
                                            <li><a href="#0" class="next">Next</a></li>
                                        </ul> <!-- .cd-timeline-navigation -->
                                    </div> <!-- .timeline -->
                                
                                    <div class="events-content" >
                                        <ol>
                                            @php
                                                $i=0;
                                            @endphp
                                            @foreach($histories as $history)
                                                @if($i==0)
                                                    <li class="selected" data-date="01/01/{{$history->year}}">
                                                @else
                                                    <li data-date="01/01/{{$history->year}}">
                                                @endif
                                                        <h2>{{$history->year}}</h2>
                                                        <div>
                                                            {!!$history->history!!}
                                                        </div>
                                                    </li>
                                                @php
                                                    $i++;
                                                @endphp
                                            @endforeach
                                        </ol>
                                    </div> <!-- .events-content -->
                                </section>
                            </div>
                         </div>
                    </div>
                </div>

                <!-- Our Partner Section -->
                <div class="section">
                    <div class="container">
                        <div class="row" id="History">
                            <div class="col-sm-12 section-description wow fadeInRight" >
                                <h2>Our Official Learning Partner</h2>
                                <div class="divider-1"><div class="line"></div></div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="container" style="height:100%;display:flex; flex-direction:column; justify-content:space-between;">
                                    <div>
                                        <a href="https://www.gojek.com"><img src="assets/img/Gojek-black.png" class="img-responsive" alt="" style="margin: auto; margin-top:20px;"></a>
                                        <h5 style="margin-top:20px;text-align:justify; line-height:1.5;margin-bottom:20px;">
                                            GOJEK is a tech company who provide on demand transport and lifestyle services that move the city. GOJEK now operates in more than 200 cities across Southeast Asia and more to come. Driven by the spirit of spreading positive social impacts through technology, GOJEK’s expansion aims to improve the quality of life of their customers by ensuring efficiency in each market and to be the solution of every day’s frustration.
                                        </h5>
                                    </div>
                                    <a style="margin-bottom:10px;"class="btn btn-info btn-lg" href="https://www.gojek.com/">See More Details</a>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col xs-12">
                                <div class="container" style="height:100%;display:flex; flex-direction:column; justify-content:space-between;">
                                    <div>
                                        <a href="https://www.gojek.com/go-academy/"><img src="assets/img/GoAcademy-black.png" class="img-responsive" alt="" style="margin: auto; margin-top:20px;"></a>
                                        <h5 style="margin-top:20px;text-align:justify; line-height:1.5;margin-bottom:20px;">
                                            Started from BNCCxGOJEK bootcamp in 2018, this year we proudly present our partnership with GO-ACADEMY, a talent incubator from GOJEK that produces world-class tech talents in Indonesia's Tech Industry. We are hopeful that this partnership will bear more tech talents in BNCC who will actively contribute in Indonesia's Tech Industry.    
                                        </h5>
                                    </div>
                                    <a style="margin-bottom:10px;" class="btn btn-info btn-lg" href="https://www.gojek.com/go-academy/">See More Details</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Testimonial Section -->

                <!-- <div class="section">
                    <div class="container-flex jumbtron paral parallax-1 darken">
                        <div class="row" id="Testimonial">
                            <div class="col-sm-12 features section-description wow fadeInRight" >
                                <h2>Testimonial</h2>
                                <div class="divider-1"><div class="line"></div></div>
                            </div>
                        </div>
                        <div class="row wow fadeInUp">
                            <div class="col-12">
                                <div class="slider-testimonial-container">
                                    <div class="slider slider-testimonial">
                                      <div class="quote-container">
                                        <div class="portrait octogon">
                                          <img src="https://media.licdn.com/media/AAIAAwDGAAAAAQAAAAAAAA1SAAAAJDMzNjc3ZWZhLTM1M2ItNGMxYi05ZDQ1LTQyMjQ3ZGYwYTM5ZQ.jpg" alt="">
                                        </div>
                                        <div class="quote">
                                          <blockquote>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                            <cite>
                                              <span>Juliana Cen</span>
                                              <br />
                                              CMO Lead - Microsoft
                                              <br />
                                              BNCC 2000 - 2003
                                            </cite>
                                          </blockquote>
                                        </div>
                                      </div>
                                      <div class="quote-container">
                                        <div class="portrait octogon">
                                          <img src="https://media.licdn.com/media/p/7/005/08f/180/0c8e950.jpg" alt="">
                                        </div>
                                        <div class="quote">
                                          <blockquote>
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                            <cite>
                                              <span>Calvin Andhika</span>
                                              <br />
                                              Founder - Chelly
                                              <br />
                                              BNCC 2013 - 2016
                                            </cite>
                                          </blockquote>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <svg>
                                    <defs>
                                      <clipPath clipPathUnits="objectBoundingBox" id="octogon">
                                        <polygon points="0.50001 0.00000, 0.61887 0.06700, 0.75011 0.06721, 0.81942 0.18444, 0.93300 0.25001, 0.93441 0.38641, 1.00000 0.49999, 0.93300 0.61887, 0.93300 0.75002, 0.81556 0.81944, 0.74999 0.93302, 0.61357 0.93444, 0.50001 1.00000, 0.38118 0.93302, 0.24998 0.93302, 0.18056 0.81556, 0.06700 0.74899, 0.06559 0.61359, 0.00000 0.49999, 0.06700 0.38111, 0.06700 0.25001, 0.18440 0.18058, 0.25043 0.06700, 0.38641 0.06559, 0.50001 0.00000"></polygon>
                                      </clipPath>
                                    </defs>
                                  </svg>
                            </div>
                         </div>
                    </div>
                </div> -->

                <!-- Contant and Maps Section -->
                <div class="section">
                    <div class="container py-5">
                        <div class="row wow fadeInUp" id="Contact">
                            <div class="col-12 col-sm-6 features section-description " >
                                <h2>Contact <span>Us</span></h2>
                                <form method="POST" action="http://formspree.io/pr@bncc.net" class="text-left">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label for="formSenderName">Name</label>
                                        <input type="text" class="form-control" name="name" placeholder="Your Name" id="formSenderName">
                                    </div>
                                    <div class="form-group">
                                        <label for="formSenderEmail">Email address</label>
                                        <input class="form-control" type="email" name="email" placeholder="Your email" id="formSenderEmail">
                                    </div>
                                    <div class="form-group">
                                        <label for="formSenderSubject">Subject</label>
                                        <input class="form-control" type="text" name="_subject" placeholder="Your subject" id="formSenderSubject">
                                    </div>
                                    <div class="form-group">
                                        <label for="formSenderMessage">Message</label>
                                        <textarea class="form-control" name="message" placeholder="Your message" id="formSenderMessage" rows="3"></textarea> 
                                    </div>
                                    <input type="hidden" name="_next" value="https://bncc.net" />
                                    <button type="submit" class="btn btn-outline-primary text-border-bncc">Send</button>
                                    </form>
                            </div>
                            <div class="col-12 col-sm-6 features section-description " >
                                <h2>Our <span>Location</span></h2>
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.455113883736!2d106.78082431476906!3d-6.20353799550915!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f6c4a8e09f5d%3A0x29720d62d8b976c5!2sBina+Nusantara+Computer+Club!5e0!3m2!1sid!2sid!4v1518551585887" width="100%" height="450px" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>    
                </div>

            <!-- Footer -->
            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-sm-6 footer-copyright float-left text-left">
                            
                            &copy; 2020 <a href="https://bncc.net">Bina Nusantara Computer Club</a>, All Right Reserved.
                            <br/>
                            Create with &#x2764; by Research and Development BNCC
                        </div>
                        <div class="col-12 col-sm-6 float-right text-right align-self-center">
                                <a class="px-1" target="_blank" href="https://www.instagram.com/bnccbinus/"><i class="fab fa-instagram fa-2x"></i></a>
                                <a class="px-1" target="_blank" href="https://www.facebook.com/bina.nusantara.computer.club/"><i class="fab fa-facebook fa-2x"></i></a>
                                <a class="px-1" target="_blank" href="https://twitter.com/bncc_binus"><i class="fab fa-twitter fa-2x"></i></a>
                            </div>
                    </div>
                </div>
            </footer>
    
            <!-- Optional JavaScript -->
            <script src="assets/js/jquery-3.3.1.min.js"></script>
            <script src="assets/js/jquery-1.11.1.min.js"></script>
            <script src="assets/js/bootstrap.bundle.min.js"></script>
            <script src="assets/js/jquery.backstretch.min.js"></script>
            <script src="assets/js/wow.min.js"></script>
            <script src="assets/js/waypoints.min.js"></script>
            <script src="assets/slick/slick.min.js"></script>
            <script src="assets/animsition/js/animsition.min.js"></script>
            <script src="assets/js/custom.js"></script>
            <script src="assets/js/timeline.js"></script>
            <script src="assets/js/index.js"></script>
            <script src="assets/js/organization.js"></script>

            <script src="js/hideseek.js" id="test"></script>
            <script type="text/javascript">
                @foreach($products as $product)
                    function change7() {
                        $("#missionTextDetail").css('opacity', '0.0');
                        $("#iconDetail").css('opacity','0.0');
                        setTimeout(() => {
                        $("#missionTextDetail").text("{{$product->product_name}}").css('opacity', '1.0');
                        $("#iconDetail").attr("src","assets/svg/mission1.svg").css("opacity","1");
                        }, 250);
                    }
                @endforeach
            </script>
        </body>
    </div>
</html>
