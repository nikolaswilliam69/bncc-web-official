<!doctype html>
<html lang="en">
    <head>
        <title>Bina Nusantara Computer Club - BNCC</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
        <!-- Framework CSS -->
            <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css"> -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Raleway:300,400,500,800">
        <link rel="stylesheet" href="../assets/fontawesome/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
            <!-- <link rel="stylesheet" href="assets/slick/slick.css"> -->
            <!-- <link rel="stylesheet" href="assets/slick/slick-theme.css">  -->
        <link rel="stylesheet" href="../assets/css/animate.css">        
        <!-- Custom CSS -->
        <link rel="stylesheet" href="../assets/css/custom.css">
        <link rel="stylesheet" href="../assets/css/test.css">
        <link rel="stylesheet" href="../assets/animsition/css/animsition.min.css">   
            <!-- <link rel="stylesheet" href="assets/css/timeline.css"> -->
            <!-- <link rel="stylesheet" href="assets/css/testimonial.css"> -->
        <link rel="stylesheet" href="../assets/css/organization.css">        
    </head>
    <div class="js animsition">
        <body onload="pageReady();" data-spy="scroll">
            <!-- <div id="preloader"></div> -->
            <!-- Header Navigation -->
            <nav class="navbar fixed-top navbar-expand-lg navbar-light">
                <a class="navbar-brand" href="{{url('/')}}">BNCC</a>             
                <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#top-navbar-1" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="top-navbar-1">
                    <ul class="navbar-nav"> 
                    </ul>
                    <ul class="navbar-nav ml-auto navbar-light navbar-scrollable">
                        <li class="nav-item"><a class="btn btn-link-3" href="{{url('/')}}">Go Back Home</a></li>
                    </ul>
                </div>
            </nav>
            
            <div class="section-container text" >
                <div class="container-fluid" >
                    <div class="row" id="WhoAreWe">
                        <div class="col-sm-12 features section-description wow fadeInRight" >
                            <h2>{{$news->title}}</h2>
                            <div class="divider-1"><div class="line"></div></div>
                        </div>
                    </div>
                    
                    <div class="row wow fadeInLeftBig">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-10">
                            <div class="col-12 justify-content-md-center">
                                <img src="../assets/img/news/{{$news->image}}" width="100%">
                            </div>
                            <div class="divider-1"><div class="line"></div></div>
                            <div class="col-12">
                                <p class="content">
                                    {!!$news->content!!}
                                </p>
                            </div>
                        </div>
                        <div class="col-sm-1"></div>
                    </div>    
                </div>
            </div>

            <!-- Footer -->
            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-sm-6 footer-copyright float-left text-left">
                            &copy; 2018 <a href="https://bncc.net">Bina Nusantara Computer Clubs</a>, All Right Reserved.
                            <br/>
                            Create with &#x2764; by Research and Development BNCC 29th
                        </div>
                        <div class="col-12 col-sm-6 float-right text-right align-self-center">
                                <a class="px-1" target="_blank" href="https://www.instagram.com/bnccbinus/"><i class="fab fa-instagram fa-2x"></i></a>
                                <a class="px-1" target="_blank" href="https://www.facebook.com/bina.nusantara.computer.club/"><i class="fab fa-facebook fa-2x"></i></a>
                                <a class="px-1" target="_blank" href="https://twitter.com/bncc_binus"><i class="fab fa-twitter fa-2x"></i></a>
                            </div>
                    </div>
                </div>
            </footer>

            <!-- Optional JavaScript -->
            <!-- <script src="assets/js/jquery-3.3.1.min.js"></script> -->
            <script src="../assets/js/jquery-1.11.1.min.js"></script>
            <script src="../assets/js/bootstrap.bundle.min.js"></script>
            <script src="../assets/js/jquery.backstretch.min.js"></script>
            <script src="../assets/js/wow.min.js"></script>
            <script src="../assets/js/waypoints.min.js"></script>
            <!-- <script src="assets/slick/slick.min.js"></script> -->
            <script src="../assets/animsition/js/animsition.min.js"></script>
            <script src="../assets/js/custom.js"></script>
            <script src="../assets/js/organization.js"></script>
            <!-- <script src="assets/js/timeline.js"></script> -->
        </body>
    </div>
</html>