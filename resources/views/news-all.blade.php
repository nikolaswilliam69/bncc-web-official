<!DOCTYPE html>
<html>
<head> 
	<title>Bina Nusantara Computer Club - BNCC</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
    <!-- Framework CSS -->
        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css"> -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Raleway:300,400,500,800">
    <link rel="stylesheet" href="assets/fontawesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <!-- <link rel="stylesheet" href="assets/slick/slick.css"> -->
        <!-- <link rel="stylesheet" href="assets/slick/slick-theme.css">  -->
    <link rel="stylesheet" href="assets/css/animate.css">        
    <!-- Custom CSS -->
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/animsition/css/animsition.min.css">
	<link rel="stylesheet" href="assets/css/test.css">
        <!-- <link rel="stylesheet" href="assets/css/timeline.css"> -->
        <!-- <link rel="stylesheet" href="assets/css/testimonial.css"> -->
    <link rel="stylesheet" href="assets/css/organization.css">
</head>
<div class="js animsition">
	<body onload="pageReady();" data-spy="scroll">
	<nav class="navbar fixed-top navbar-expand-lg navbar-light">
		<a class="navbar-brand" href="{{url('/')}}">BNCC</a>             
		<button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#top-navbar-1" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="top-navbar-1">
			<ul class="navbar-nav"> 
			</ul>
			<ul class="navbar-nav ml-auto navbar-light navbar-scrollable">
				<li class="nav-item"><a class="btn btn-link-3" href="{{url('/')}}">Go Back Home</a></li>
			</ul>
		</div>
	</nav>

	{{-- last and first 4 --}}
	<div class="section-container text">
		<div class="container-fluid">
			<div class="row" id="news">
				<div class="col-sm-12 text-center features section-description">
					<h2>News</h2>
					<div class="divider-1"><div class="line"></div></div>
				</div>
			</div>

			<div class="row wow fadeInUp py-5 animated">
				<div class="col-lg-6">
					<div class="text-left">
						<div class="card-img card-last">
							@foreach($news as $report)
								@if($news->last() == $report)
									<img class="card-img-top img-fluid" src="assets/img/news/{{$report->image}}">
									<div class="carousel-caption">
										<h5 class="text-lg-left text-md-left text-sm-center tile-news-top">
											{{$report->title}}
										</h5>

										<div class="text-lg-left text-md-left text-sm-center ">
											<a href="{{route('news.show',['news'=>$report->id])}}">
												<button type="button" class="btn btn-primary">Click For More</button>
											</a>
										</div>
									</div>
								@endif
							@endforeach
						</div>
					</div>
				</div>

				<div class="col-lg-6">
					<div class="row card1">
						@foreach($news as $report)
							@if($i<=2)
								@if($news->last() != $report)
									<div class="col-lg-6 col-md-6">
										<div class="text-left">
											<div class="card-img card-small">
												<img class="card-img-top img-fluid" src="assets/img/news/{{$report->image}}">
												<div class="carousel-caption">
													<h5 class="text-lg-left text-md-left text-sm-center tile-news-top">
														{{$report->title}}
													</h5>

													<div class="text-lg-left text-md-left text-sm-center ">
														<a href="{{route('news.show',['news'=>$report->id])}}">
															<button type="button" class="btn btn-primary">Click For Mores</button>
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
									@php
										$i++
									@endphp
								@endif
							@elseif($i<=3||$i<=4)
								@if($news->last() != $report)
									<div class="col-lg-6 col-md-6">
										<div class="text-left">
											<div class="card-img card-small">
												<img class="card-img-top img-fluid" src="assets/img/news/{{$report->image}}">
												<div class="carousel-caption">
													<h5 class="text-lg-left text-md-left text-sm-center tile-news-top">
														{{$report->title}}
													</h5>

													<div class="text-lg-left text-md-left text-sm-center">
														<a href="{{route('news.show',['news'=>$report->id])}}">
															<button type="button" class="btn btn-primary">Click For More</button>
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
									@php
										$i++
									@endphp
								@endif	
							@endif
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>

	{{-- the rest news --}}
	<div class="section-container text" >
		<div class="container-fluid">
			<div class="row" >

				{{-- left side --}}
				<div class="col-lg-6 col-md-6 col-sm-6">
					@php
						$i=1;
					@endphp
					@foreach($news as $report)
						@if($news->last() != $report)
							@if($i >= 5)
								@if($i%2 == 1)
									<div class="row" style="margin-top: 30px;">
										<div class="col-lg-6">
											<a href=""><img class="card-img-top img-fluid" src="assets/img/news/{{$report->image}}"></a>  
										</div>
											<div class="col-lg-6">
											<h6 class="card-title">{{$report->title}}</h6>
											<p class="card-text">
												{!!substr($report->content,0,250)!!} ...
											</p>
											<a href="{{route('news.show',['news'=>$report->id])}}" class="btn btn-outline-primary" role="button" aria-pressed="true" style="margin-bottom: 25px;">View More</a>
										</div>
									</div>
								@endif
							@endif
						@endif
						@php
							$i++
						@endphp
					@endforeach
				</div>

				{{-- right side --}}
				<div class="col-lg-6 col-md-6 col-sm-6">
					@php
						$i=1;
					@endphp
					@foreach($news as $report)
						@if($news->last() != $report)
							@if($i >= 5)
								@if($i%2 == 0)
									<div class="row" style="margin-top: 30px;">
										<div class="col-lg-6">
											<a href=""><img class="card-img" src="assets/img/news/{{$report->image}}"></a>  
										</div>
											<div class="col-lg-6">
											<h6 class="card-title">{{$report->title}}</h6>
											<p class="card-text">
												{!!substr($report->content,0,250)!!} ...
											</p>
											<a href="{{route('news.show',['news'=>$report->id])}}" class="btn btn-outline-primary" role="button" aria-pressed="true" style="margin-bottom: 25px;">View More</a>
										</div>
									</div>
								@endif
							@endif
						@endif
						@php
							$i++
						@endphp
					@endforeach
				</div>
			</div>
		</div>
	</div>
		<!-- Optional JavaScript -->
        <!-- <script src="assets/js/jquery-3.3.1.min.js"></script> -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/waypoints.min.js"></script>
        <!-- <script src="assets/slick/slick.min.js"></script> -->
        <script src="assets/animsition/js/animsition.min.js"></script>
        <script src="assets/js/custom.js"></script>
        <script src="assets/js/organization.js"></script>
        <!-- <script src="assets/js/timeline.js"></script> -->
		<script type="text/javascript" src="assets/js/test.js"></script>
	</body>
</div>
</html>
