@extends('adminlte::page')

@section('title', 'History - Update')

@section('content_header')
    <h1>Update History</h1>
@stop

@section('content')
    <form action="{{route('history.update',['history'=>$history->id])}}" method="POST">
    	{{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
    	<div class="row">
    		<div class="col-sm-10">
		    	<div class="input-group">
		    		<span class="input-group-addon">Year</span>
		    		<input class="form-control" type="text" name="year" id="year" pattern="[0-9]{4,}" title="Must contain 4 digit of number" value="{{$history->year}}" required>
		    	</div>
    		</div>
    	</div>

    	<div class="row">
    		<div class="col-sm-10">
		    	<div class="input-group">
					<span class="input-group-addon">Description</span>
					<textarea class="form-control" name="history" id="history" rows="4" required>{{$history->history}}</textarea>
				</div>
    		</div>
    	</div>
    	
    	<div class="row">
    		<div class="text-center col-sm-10">
		    	<div class="input-group">
					<input class="btn btn-primary" type="submit" name="submit">
		    	</div>
    		</div>
    	</div>	
    </form>
@stop

@section('js')
    <script type="text/javascript" src="../../public/vendor/ckeditor/ckeditor.js"></script>
    <script type="text/javascript">
        $(function(){
            CKEDITOR.replace('history')
        })
    </script>
@stop