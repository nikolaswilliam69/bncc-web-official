@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Add Team</h1>
@stop

@section('content')
    <form enctype="multipart/form-data" action="{{route('team.store')}}" method="POST">
    	{{csrf_field()}}

    	<div class="row">
    		<div class="col-sm-4">
    			<div class="input-group">
    				<span class="input-group-addon">
    					Image
    				</span>
    				<input class="form-control" type="file" name="profile_photo" id="profile_photo" accept="image/*" required>
    			</div>
    		</div>

    		<div class="col-sm-1"></div>

    		<div class="col-sm-4">
    			<div class="input-group">
    				<span class="input-group-addon">
    					Name
    				</span>
    				<input class="form-control" type="text" name="profile_name" id="profile_name">
    			</div>
    		</div>    		
    	</div>

    	<br>

    	<div class="row">
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon">
                        Division
                    </span>
                    <select class="form-control" type="text" name="profile_division" id="profile_division" required oninput="sub()">
                        <option disabled selected>-- Choose One --</option>
                        <option>DPI</option>
                        <option>Marketing</option>
                        <option>Product</option>
                        <option>Internal</option>
                    </select>
                </div>
            </div>

            <div class="col-sm-1"></div>

    		<div class="col-sm-4">
    			<div class="input-group">
                    <span class="input-group-addon">
                        Sub Division
                    </span>
                    <select class="form-control" type="text" name="profile_sub_division" id="profile_sub_division">
                        <option disabled selected>-- Choose One --</option>
                        {{-- dpi selection --}}
                        <option class="DPI">-</option>

                        {{-- marketing --}}
                        <option class="Marketing">External Event Organizer</option>
                        <option class="Marketing">Public Relation</option>

                        {{-- product --}}
                        <option class="Product">Learning and Training</option>
                        <option class="Product">Fave Solution</option>
                        <option class="Product">File Magz</option>

                        {{-- internal --}}
                        <option class="Internal">Research And Development</option>
                        <option class="Internal">Human Resource Development</option>                    
                    </select>
                </div>
            </div>
    	</div>

        <br>

        <div class="row">
            <div class="col-sm-9 text-center">
                <div class="input-group">
                    <span class="input-group-addon">
                        Position
                    </span>
                    <select class="form-control" name="profile_position" id="profile_position" required>
                        <option disabled selected>-- Choose One --</option>
                        {{-- dpi selection --}}
                        <option class="DPI">Chief Executive Officer</option>
                        <option class="DPI">Chief Finance Officer</option>
                        <option class="DPI">Chief Marketing Officer</option>
                        <option class="DPI">Chief Product Officer</option>
                        <option class="DPI">Chief Internal Officer</option>

                        {{-- SubDiv selection --}}
                        <option class="sub">Manager</option>
                        <option class="sub">Staff</option>
                    </select>
                </div>
            </div>
        </div>

    	<br>

    	<div class="row text-center">
    		<div class="col-sm-9">
    			<button type="submit" class="btn btn-primary">
    				Submit
    			</button>
    		</div>
    	</div>
    </form>
@stop


@section('js')
    <script type="text/javascript">
        $('.DPI').hide();
        $('.Marketing').hide();
        $('.Product').hide();
        $('.Internal').hide();
        $('.sub').hide();

        function sub(){
            $('.DPI').hide();
            $('.Marketing').hide();
            $('.Product').hide();
            $('.Internal').hide();
            $('.sub').hide();

            $('#profile_sub_division').prop('selectedIndex',0);

            var select = document.getElementById('profile_division').value;

            if(select == 'DPI'){
                $('#profile_sub_division').prop('selectedIndex',1);
                $('#profile_position').prop('selectedIndex',0);
                $('.'+select).show();
            }
            else{
                $('#profile_sub_division').prop('selectedIndex',0);
                $('#profile_position').prop('selectedIndex',0);
                $('.'+select).show();
                $('.sub').show();
            }
        }

    </script>
@stop