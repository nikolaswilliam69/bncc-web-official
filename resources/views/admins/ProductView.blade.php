@extends('adminlte::page')

@section('title', 'View Product')

@section('content_header')
	<h1>View Product</h1>
@stop

@section('content')
	<div class="container">
		<div class="row" style="border-bottom: solid black; background-color: grey;">
			<div class="col-sm-4">
				<h4>Logo</h4>
			</div>
			<div class="col-sm-2">
				<h4>Name</h4>
			</div>
			<div class="col-sm-2">
				<h4>Website</h4>
			</div>
			<div class="col-sm-2">
				<h4></h4>
			</div>
		</div>

		@foreach($products as $product)
			<div class="row" style="padding: 5px; background-color: grey;">
				<div class="col-sm-4">
					<img src="assets/img/products/{{$product->product_logo}}" style="width: 300px;">
				</div>
				<div class="col-sm-2">
					{{$product->product_name}}
				</div>
				<div class="col-sm-2">
					{{$product->product_website}}
				</div>
				<div class="col-sm-2">
					<div class="row">
						<div class="col-sm-6">
							<a href="{{route('product.edit',['product'=>$product->id])}}" class="btn btn-primary">Edit</a>
						</div>
						<div class="col-sm-6">
							<form action="{{ route('product.destroy', ['product'=>$product->id]) }}" method='POST'>

								{{ csrf_field() }}

								<input type="hidden" name="_method" value="DELETE">
								<input type="submit" class="btn btn-danger red" value="delete">
								
							</form>
						</div>
					</div>
				</div>
			</div>
		@endforeach
	</div>
@stop