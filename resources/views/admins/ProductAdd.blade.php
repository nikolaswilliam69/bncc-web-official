@extends('adminlte::page')

@section('title','Add Product')

@section('content_header')
    <h1>Add Product</h1>
@stop

@section('content')	
	<form enctype="multipart/form-data" action="{{route('product.store')}}" method="POST">
		{{csrf_field()}}
		<div class="row ">
			<div class="col-sm-3">
				<div class="input-group">
					<span class="input-group-addon">Name</i></span>
					<input class="form-control" type="text" name="product_name" id="product_name">
				</div>
			</div>

			<div class="col-sm-1"></div>

			<div class="col-sm-4">
				<div class="input-group">
					<span class="input-group-addon">Logo</i></span>
					<input class="form-control" type="file" name="product_logo" id="product_logo" accept="image/*">
				</div>
			</div>

			<div class="col-sm-1"></div>

			<div class="col-sm-3">
				<div class="input-group">
					<span class="input-group-addon">website</i></span>
					<input class="form-control" type="url" name="product_website" id="product_website" value="https://">
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-6">
				<div class="input-group">
					<span class="input-group-addon">Description</i></span>
					<textarea class="form-control" name="product_about"></textarea>
				</div>
			</div>

			<div class="col-sm-1"></div>

			<div class="col-sm-5">
				<div class="input-group">
					<span class="input-group-addon">Product Main Color</i></span>
					<input class="form-control" type="text" name="product_color" id="product_color" value="#" oninput="change()">
				</div>
				<div style="height: 15px;" id="color"></div>
			</div>
		</div>
		
		<br>
		<div class="row text-center">
			<input type="submit" name="submit" class="btn btn-primary">
		</div>
	</form>
@stop

@section('js')
	<script type="text/javascript">
		function change(){
			var test = $('input[id="product_color"]').val();
			$('div[id="color"]').css('background-color', test);
		}
	</script>
@stop