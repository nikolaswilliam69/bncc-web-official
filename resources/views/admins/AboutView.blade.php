@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>View About Us</h1>
@stop

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-2 text-center">Type</div>
			<div class="col-sm-8 text-center">Content</div>
			<div class="col-sm-2 text-center">Action</div>
		</div>
		@if($abouts->isEmpty())
		<div class="row">
			<div class="col-sm-10 text-center">
				<b>No data Found</b>	
			</div>
		</div>
		@else
		    @foreach($abouts as $about)
			    <div class="row">
			    	<div class="col-sm-2 text-center">{{$about->type}}</div>
					<div class="col-sm-8">{!!$about->content!!}</div>
					<div class="col-sm-2 text-center">
						<div class="col-sm-6">
							<a href="{{route('about.edit',['about'=>$about->id])}}" class="btn btn-primary">Edit</a>
						</div>
						<div class="col-sm-6">
							<form action="{{ route('about.destroy', ['about'=>$about->id]) }}" method='POST'>

								{{ csrf_field() }}

								<input type="hidden" name="_method" value="DELETE">
								<input type="submit" class="btn btn-danger red" value="delete">
								
							</form>
						</div>
					</div>
			    </div>
			    <br>
			@endforeach
		@endif
	</div>
@stop