@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Edit Team</h1>
@stop

@section('content')
    <form enctype="multipart/form-data" action="{{ route('team.update', [$team->id]) }}" method="POST">
    	{{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
    	<div class="row">
    		<div class="col-sm-4">
    			<div class="input-group">
    				<span class="input-group-addon">
    					Image
    				</span>
    				<input class="form-control" type="file" name="profile_photo" id="profile_photo" accept="image/*" required>
    			</div>
    		</div>

    		<div class="col-sm-1"></div>

    		<div class="col-sm-4">
    			<div class="input-group">
    				<span class="input-group-addon">
    					Name
    				</span>
    				<input class="form-control" type="text" name="profile_name" id="profile_name" value="{{$team->profile_name}}">
    			</div>
    		</div>    		
    	</div>

    	<br>

    	<div class="row">
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon">
                        Division
                    </span>
                    <select class="form-control" type="text" name="profile_division" id="profile_division" required oninput="sub()">
                        <option disabled>-- Choose One --</option>

                        @if($team->profile_division == 'DPi')
                            <option selected>
                        @else
                            <option>
                        @endif
                            DPI
                        </option>

                        @if($team->profile_division == 'Marketing')
                            <option selected>
                        @else
                            <option>
                        @endif
                            Marketing
                        </option>

                        @if($team->profile_division == 'Product')
                            <option selected>
                        @else
                            <option>
                        @endif
                            Product
                        </option>

                        @if($team->profile_division == 'Internal')
                            <option selected>
                        @else
                            <option>
                        @endif
                            Internal
                        </option>
                    </select>
                </div>
            </div>

            <div class="col-sm-1"></div>

    		<div class="col-sm-4">
    			<div class="input-group">
                    <span class="input-group-addon">
                        Sub Division
                    </span>
                    <select class="form-control" type="text" name="profile_sub_division" id="profile_sub_division">
                        <option disabled>-- Choose One --</option>
                        {{-- dpi selection --}}
                        @if($team->profile_division == 'DPI')
                            <option class="DPI" selected="">
                        @else
                            <option class="DPI">
                        @endif
                            -
                        </option>

                        {{-- marketing --}}
                        @if($team->profile_sub_division == 'External Event Organizer')
                            <option class="Marketing" selected="">
                        @else
                            <option class="Marketing">
                        @endif
                            External Event Organizer
                        </option>

                         @if($team->profile_sub_division == 'Public Relation')
                            <option class="Marketing" selected="">
                        @else
                            <option class="Marketing">
                        @endif
                            Public Relation
                        </option>

                        {{-- product --}}
                        @if($team->profile_sub_division == 'Learning and Training')
                            <option class="Product" selected="">
                        @else
                            <option class="Product">
                        @endif
                            Learning and Training
                        </option>

                        @if($team->profile_sub_division == 'Fave Solution')
                            <option class="Product" selected="">
                        @else
                            <option class="Product">
                        @endif
                            Fave Solution
                        </option>

                        @if($team->profile_sub_division == 'File Magz')
                            <option class="Product" selected="">
                        @else
                            <option class="Product">
                        @endif
                            File Magz
                        </option>

                        {{-- internal --}}
                        @if($team->profile_sub_division == 'Research And Development')
                            <option class="Internal" selected="">
                        @else
                            <option class="Internal">
                        @endif
                            Research And Development
                        </option>

                         @if($team->profile_sub_division == 'Human Resource Development')
                            <option class="Internal" selected="">
                        @else
                            <option class="Internal">
                        @endif
                            Human Resource Development
                        </option>                  
                    </select>
                </div>
            </div>
    	</div>

        <br>

        <div class="row">
            <div class="col-sm-9 text-center">
                <div class="input-group">
                    <span class="input-group-addon">
                        Position
                    </span>
                    <select class="form-control" name="profile_position" id="profile_position" required>
                        <option disabled>-- Choose One --</option>
                        {{-- dpi selection --}}
                        @if($team->profile_position == 'Chief Executive Officer')
                            <option class="DPI" selected>
                        @else
                            <option class="DPI">
                        @endif
                            Chief Executive Officer
                        </option>

                        @if($team->profile_position == 'Chief Finance Officer')
                            <option class="DPI" selected>
                        @else
                            <option class="DPI">
                        @endif
                            Chief Finance Officer
                        </option>

                        @if($team->profile_position == 'Chief Marketing Officer')
                            <option class="DPI" selected>
                        @else
                            <option class="DPI">
                        @endif
                            Chief Marketing Officer
                        </option>

                        @if($team->profile_position == 'Chief Product Officer')
                            <option class="DPI" selected>
                        @else
                            <option class="DPI">
                        @endif
                            Chief Product Officer
                        </option>

                        @if($team->profile_position == 'Chief Internal Officer')
                            <option class="DPI" selected>
                        @else
                            <option class="DPI">
                        @endif
                            Chief Internal Officer
                        </option>

                        {{-- SubDiv selection --}}
                        @if($team->profile_position == 'Manager')
                            <option class="sub" selected>
                        @else
                            <option class="sub">
                        @endif
                            Manager
                        </option>

                        @if($team->profile_position == 'Staff')
                            <option class="sub" selected>
                        @else
                            <option class="sub">
                        @endif
                            Staff
                        </option>

                    </select>
                </div>
            </div>
        </div>

    	<br>

    	<div class="row text-center">
    		<div class="col-sm-9">
    			<button type="submit" class="btn btn-primary">
    				Submit
    			</button>
    		</div>
    	</div>
    </form>
@stop


@section('js')
    <script type="text/javascript">
        $('.DPI').hide();
        $('.Marketing').hide();
        $('.Product').hide();
        $('.Internal').hide();
        $('.sub').hide();

        function sub(){
            $('.DPI').hide();
            $('.Marketing').hide();
            $('.Product').hide();
            $('.Internal').hide();
            $('.sub').hide();

            $('#profile_sub_division').prop('selectedIndex',0);

            var select = document.getElementById('profile_division').value;

            if(select == 'DPI'){
                $('#profile_sub_division').prop('selectedIndex',1);
                $('#profile_position').prop('selectedIndex',0);
                $('.'+select).show();
            }
            else{
                $('#profile_sub_division').prop('selectedIndex',0);
                $('#profile_position').prop('selectedIndex',0);
                $('.'+select).show();
                $('.sub').show();
            }
        }

    </script>
@stop