@extends('adminlte::page')

@section('content_header')
	View Events
@stop

@section('content')
	<div class="row text-center">
		<div class="col-sm-2">
			Poster
		</div>
		<div class="col-sm-2">
			Title
		</div>
		<div class="col-sm-2">
			Date
		</div>
		<div class="col-sm-2">
			Time
		</div>
		<div class="col-sm-2">
			Action
		</div>
	</div>

	@if($events->isEmpty())
		No Event Registered
	@else
		@foreach($events as $event)
		<div class="row text-center">
			<div class="col-sm-2">
				<img src="assets/img/events/{{$event->poster}}" style="width: 150px;">
			</div>
			<div class="col-sm-2">
				{{$event->title}}
			</div>
			<div class="col-sm-2">
				@if($event->date_end == NULL)
                    {{date('d F Y', strtotime($event->date_start))}}
                @else
               		{{date('d', strtotime($event->date_start))}} - {{date('d F Y', strtotime($event->date_end))}}
                @endif
			</div>
			<div class="col-sm-2">
				{{date('H:i',strtotime($event->time_start))}} - {{date('H:i',strtotime($event->time_end))}}
			</div>
			<div class="col-sm-2">
				<div class="row">
					<div class="col-sm-6">
						<a href="{{route('event.edit',['event'=>$event->id])}}" class="btn btn-primary">Edit</a>
					</div>
					<div class="col-sm-6">
						<form action="{{ route('event.destroy', ['event'=>$event->id]) }}" method='POST'>
							{{ csrf_field() }}
							<input type="hidden" name="_method" value="DELETE">
							<input type="submit" class="btn btn-danger red" value="delete">
						</form>
					</div>
				</div>
			</div>
		</div>
		@endforeach
	@endif
@stop