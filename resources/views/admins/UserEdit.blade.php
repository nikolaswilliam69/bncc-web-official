@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Edit Profile</h1>
@stop

@section('content')
    <form action="{{route('user.update',['user'=>$user->id])}}" method="POST">
    	{{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
    	<div class="row">
    		<div class="col-sm-5">
		    	<div class="input-group">
		    		<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
		    		<input class="form-control" type="text" name="email" id="email" value="{{$user->email}}" required>
		    	</div>
    		</div>
    	</div>

    	<div class="row">
    		<div class="col-sm-5">
		    	<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
					<input class="form-control" type="password" name="password" id="password" required>
				</div>
    		</div>
    	</div>
    	<br>
    	
    	<div class="row">
    		<div class="text-center col-sm-10">
		    	<div class="input-group">
					<input class="btn btn-primary" type="submit" name="submit">
		    	</div>
    		</div>
    	</div>	
    </form>
@stop
