@extends('adminlte::page')

@section('content_header')
	<h1>Add News</h1>
@stop

@section('content')
	<form enctype="multipart/form-data" action="{{route('news.store')}}" method="POST">
		{{csrf_field()}}
		<div class="input-group">
			<span class="input-group-addon">image</span>
			<input class="form-control" type="file" name="image" id="image" accept="image/*" required>
		</div>

		<div class="input-group">
			<span class="input-group-addon"><i class="glyphicon glyphicon-header"></i></span>
			<input class="form-control" type="text" name="title" id="title" required="">
		</div>

		<div class="input-group">
			<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
			<input class="form-control" type="date" name="date" id="date" required="">
		</div>

		<div class="input-group">
			<span class="input-group-addon">Description</span>
			<textarea class="form-control content-news" name="content" id="content"></textarea>
		</div>

		<input type="submit" name="submit" class="btn btn-primary">
	</form>
@stop

@section('js')
    <script type="text/javascript" src="../public/vendor/ckeditor/ckeditor.js"></script>
    <script type="text/javascript">
        $(function(){
            CKEDITOR.replace('content')
        })
    </script>
@stop