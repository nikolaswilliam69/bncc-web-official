@extends('adminlte::page')

@section('title', 'Add About Us')

@section('content_header')
    <h1>Add About Us</h1>
@stop

@section('content')
    <form action="{{route('about.store')}}" method="POST">
    	{{csrf_field()}}
    	<div class="row">
    		<div class="col-sm-10">
		    	<div class="input-group">
		    		<span class="input-group-addon">Type</span>
		    		<select class="form-control" name="type" id="type" required>
		    			<option selected disabled>-- Choose One --</option>
		    			<option>Who Are We</option>
		    			<option>Vission</option>
		    		</select>
		    	</div>
    		</div>
    	</div>

    	<div class="row">
    		<div class="col-sm-10">
		    	<div class="input-group">
					<span class="input-group-addon">Description</span>
					<textarea class="form-control content-news" name="content" id="content"></textarea>
				</div>
    		</div>
    	</div>
    	
    	<div class="row">
    		<div class="text-center col-sm-10">
		    	<div class="input-group">
					<input class="btn btn-primary" type="submit" name="submit">
		    	</div>
    		</div>
    	</div>	
    </form>
@stop


@section('js')
    <script type="text/javascript" src="../public/vendor/ckeditor/ckeditor.js"></script>
    <script type="text/javascript">
        $(function(){
            CKEDITOR.replace('content')
        })
    </script>
@stop