@extends('adminlte::page')

@section('content_header')
	<h1>Add Event</h1>
@stop

@section('content')
	<form enctype="multipart/form-data" action="{{route('event.update',['event'=>$event->id])}}" method="POST">
		{{csrf_field()}}
		<input type="hidden" name="_method" value="PUT">
		<div class="input-group">
			<span class="input-group-addon">Poster</span>
			<input class="form-control" type="file" name="poster" id="poster" accept="image/*" required>
		</div>
		
		<div class="input-group">
			<span class="input-group-addon"><i class="glyphicon glyphicon-header"></i></span>
			<input class="form-control" type="text" name="title" id="title" value="{{$event->title}}" required>
		</div>
		
		<div class="row">
			<div class="col-sm-6">
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i> Start</span>
					<input class="form-control" type="date" name="date_start" id="date_start" value="{{$event->date_start}}" required>
				</div>
			</div>

			<div class="col-sm-6">
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i> End</span>
					@if($event->date_end == null)
						<input class="form-control" type="date" name="date_end" id="date_end">
					@else
						<input class="form-control" type="date" name="date_end" id="date_end" value="{{$event->date_end}}">
					@endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i> Start</span>
					<input class="form-control" type="time" name="time_start" id="time_start" value="{{$event->time_start}}" required>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i> End</span>
					<input class="form-control" type="time" name="time_end" id="time_end" value="{{$event->time_end}}" required>
				</div>
			</div>
		</div>



		<br>
		<input class="btn btn-primary" type="submit" name="submit">
	</form>
@stop