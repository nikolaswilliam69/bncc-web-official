@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>View Team</h1>
@stop

@section('content')
	<div class="row text-center">
		<div class="col-sm-2">
			Profile Photo
		</div>
		<div class="col-sm-2">Name</div>
        <div class="col-sm-2">Sub-Division</div>
		<div class="col-sm-2">Division</div>
		<div class="col-sm-2">Position</div>
        <div class="col-sm-2">Action</div>
	</div>
    @foreach($teams as $team)
    	<div class="row text-center">
    		<div class="col-sm-2">
    			<img src="assets/img/teams/{{$team->profile_photo}}" width="100%">
    		</div>
    		<div class="col-sm-2">{{$team->profile_name}}</div>
    		<div class="col-sm-2">{{$team->profile_sub_division}}</div>
    		<div class="col-sm-2">{{$team->profile_division}}</div>
    		<div class="col-sm-2">{{$team->profile_position}}</div>
            <div class="col-sm-2">
                <div class="row">
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="{{route('team.edit',['team'=>$team->id])}}" class="btn btn-primary">Edit</a>
                        </div>
                        <div class="col-sm-6">
                            <form action="{{ route('team.destroy', ['team'=>$team->id]) }}" method='POST'>

                                {{ csrf_field() }}

                                <input type="hidden" name="_method" value="DELETE">
                                <input type="submit" class="btn btn-danger red" value="delete">
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    	</div>
    @endforeach
@stop