@extends('adminlte::page')

@section('content_header')
	<h1>Edit News</h1>
@stop

@section('content')
	<form action="{{route('news.update',['news'=>$news->id])}}" method="POST" enctype="multipart/form-data">
		{{csrf_field()}}
		<input type="hidden" name="_method" value="PUT">

		<div class="row">
			<div class="col-sm-10">
				<div class="input-group">
					<span class="input-group-addon">image</span>
					<input class="form-control" type="file" name="image" id="image" accept="image/*" required>
				</div>		
			</div>
			<div class="col-sm-2">
				<p class="text-danger">*size must less then 1MB</p>
			</div>
		</div>

		

		<div class="input-group">
			<span class="input-group-addon"><i class="glyphicon glyphicon-header"></i></span>
			<input class="form-control" type="text" name="title" id="title" value="{{$news->title}}">
		</div>

		<div class="input-group">
			<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
			<input class="form-control" type="date" name="date" id="date" value="{{$news->date}}">
		</div>

		<div class="input-group">
			<span class="input-group-addon">Description</span>
			<textarea class="form-control content-news" name="content" id="content">{{$news->content}}</textarea>
		</div>

		<input type="submit" name="submit" class="btn btn-primary">
	</form>
@stop

@section('js')
    <script type="text/javascript" src="../../public/vendor/ckeditor/ckeditor.js"></script>
    <script type="text/javascript">
        $(function(){
            CKEDITOR.replace('content')
        })
    </script>
@stop