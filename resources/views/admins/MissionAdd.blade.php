@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Add Mission</h1>
@stop

@section('content')
    <form enctype="multipart/form-data" action="{{route('mission.store')}}" method="POST">
		{{csrf_field()}}
		<div class="row ">
			<div class="col-sm-5">
				<div class="input-group">
					<span class="input-group-addon">Logo</span>
					<input class="form-control" type="file" name="mission_logo" id="mission_logo" accept="image/*" required>
				</div>
			</div>

			<div class="col-sm-1"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-5">
				<div class="input-group">
					<span class="input-group-addon">Description</span>
					<textarea class="form-control" name="history" required></textarea>
				</div>
			</div>
		</div>
		
		<br>
		<div class="row text-center">
			<div class="col-sm-5">
				<input type="submit" name="submit" class="btn btn-primary">
			</div>
		</div>
	</form>
@stop
