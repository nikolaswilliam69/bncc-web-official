@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
	<div class="row" style="margin-bottom: 10px;">
		<div class="col-sm-3 text-center">
			Year
		</div>
		<div class="col-sm-6 text-center">
			History
		</div>
        <div class="col-sm-3 text-center">
            Action
        </div>
	</div>
    @foreach($histories as $history)
    	<div class="row" style="margin-bottom: 10px;">
    		<div class="col-sm-3 text-center">
    			{{$history->year}}
    		</div>	

    		<div class="col-sm-6">
                <p>
                    {!!$history->history!!}
                </p>
    		</div>

            <div class="col-sm-3 text-center">
			<div class="row">
                    <div class="col-sm-6">
                        <a href="{{route('history.edit',['history'=>$history->id])}}" class="btn btn-primary">Edit</a>
                    </div>
                    <div class="col-sm-6">
                        <form action="{{ route('history.destroy', ['history'=>$history->id]) }}" method='POST'>

                            {{ csrf_field() }}

                            <input type="hidden" name="_method" value="DELETE">
                            <input type="submit" class="btn btn-danger red" value="delete">
                            
                        </form>
                    </div>
                </div>
            </div>
    	</div>
    @endforeach
@stop