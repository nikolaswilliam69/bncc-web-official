@extends('adminlte::page')

@section('content_header')
	<h1>
		View Events
	</h1>
@stop

@section('content')
	<div class="row text-center">
		<div class="col-sm-2">
			Image
		</div>
		<div class="col-sm-2">
			Title
		</div>
		<div class="col-sm-2">
			Date
		</div>
		<div class="col-sm-2">
			Content
		</div>
		<div class="col-sm-2">
			Action
		</div>
	</div>

	@if($news->isEmpty())
		No News Submitted
	@else
		@foreach($news as $news)
		<div class="row text-center">
			<div class="col-sm-2">
				<img src="assets/img/news/{{$news->image}}" style="width: 150px;">
			</div>
			<div class="col-sm-2">
				{{$news->title}}
			</div>
			<div class="col-sm-2">
				{{date('d F Y', strtotime($news->date))}}
			</div>
			<div class="col-sm-2">
				{!! substr($news->content,0,100) !!}
			</div>
			<div class="col-sm-2">
				<div class="row">
					<div class="col-sm-6">
						<a href="{{route('news.edit',['news'=>$news->id])}}" class="btn btn-primary">Edit</a>
					</div>
					<div class="col-sm-6">
						<form action="{{ route('news.destroy', ['news'=>$news->id]) }}" method='POST'>

                            {{ csrf_field() }}

                            <input type="hidden" name="_method" value="DELETE">
                            <input type="submit" class="btn btn-danger red" value="delete">
                            
                        </form>
					</div>
				</div>
			</div>
		</div>
		@endforeach
	@endif
@stop