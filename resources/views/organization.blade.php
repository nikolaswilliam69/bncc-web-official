<!doctype html>
<html lang="en">
    <head>
        <title>Bina Nusantara Computer Club - BNCC</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
        <!-- Framework CSS -->
            <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css"> -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Raleway:300,400,500,800">
        <link rel="stylesheet" href="assets/fontawesome/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
            <!-- <link rel="stylesheet" href="assets/slick/slick.css"> -->
            <!-- <link rel="stylesheet" href="assets/slick/slick-theme.css">  -->
        <link rel="stylesheet" href="assets/css/animate.css">        
        <!-- Custom CSS -->
        <link rel="stylesheet" href="assets/css/custom.css">
        <link rel="stylesheet" href="assets/animsition/css/animsition.min.css">   
            <!-- <link rel="stylesheet" href="assets/css/timeline.css"> -->
            <!-- <link rel="stylesheet" href="assets/css/testimonial.css"> -->
        <link rel="stylesheet" href="assets/css/organization.css">
    </head>
    <div class="js animsition">
        <body onload="pageReady();" data-spy="scroll">
            <!-- <div id="preloader"></div> -->
            <!-- Header Navigation -->
            <nav class="navbar fixed-top navbar-expand-lg navbar-light">
                <a class="navbar-brand" href="{{url('/')}}">BNCC</a>             
                <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#top-navbar-1" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="top-navbar-1">
                    <ul class="navbar-nav"> 
                    </ul>
                    <ul class="navbar-nav ml-auto navbar-light navbar-scrollable">
                        <li class="nav-item"><a class="nav-link" href="#structure">Organization Structure</a></li>
                        <li class="nav-item"><a class="nav-link" href="#bom">Board Of Management</a></li>
                        <li class="dropdown open"><a class="nav-link dropdown-toggle" href="#" id="DropdownMarketing" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Marketing</a>
                            <div class="dropdown-menu" aria-labelledby="DropdownMarketing">
                                <a class="dropdown-item" href="#eeo">External Event Organizer</a>
                                <a class="dropdown-item" href="#pr">Public Relation</a>
                            </div>
                        </li>
                        <li class="dropdown open"><a class="nav-link dropdown-toggle" href="#" id="DropdownProduct" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Product</a>
                            <div class="dropdown-menu" aria-labelledby="DropdownProduct">
                                <a class="dropdown-item" href="#lnt">Learning and Training</a>
                                <a class="dropdown-item" href="#fave">Fave</a>
                                <a class="dropdown-item" href="#file">File</a>
                            </div>
                        </li>
                        <li class="dropdown open"><a class="nav-link dropdown-toggle" href="#" id="DropdownInternal" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Internal</a>
                            <div class="dropdown-menu" aria-labelledby="DropdownInternal">
                                <a class="dropdown-item" href="#rnd">Research and Dev. </a>
                                <a class="dropdown-item" href="#hrd">Human Resource Dev.   </a>
                            </div>
                        </li>
                        <li class="nav-item"><a class="btn btn-link-3" href="{{url('/')}}">Go Back Home</a></li>
                    </ul>
                </div>
            </nav>
            
            <!-- Organization Structure -->
            <div class="section-container text" >
                <div class="container" >
                    <div class="row" id="structure">
                        <div class="col-sm-12 features section-description wow fadeInRight" >
                            <h2>Organization <span> Structure</span></h2>
                            <div class="divider-1"><div class="line"></div></div>
                        </div>
                    </div>
                    
                    <!--<div class="row justify-content-md-center wow fadeInLeftBig">
                        <div class="col-12">
                            <img src="assets/img/organization/StrukturBNCC2019.png" alt="organization structure" class="img-fluid">
                        </div>
                    </div>-->
                </div>
            </div>
                
            <!-- Broad of Management -->
            <div class="section-container text" >
                <div class="container-fluid" >
                    <div class="row" id="bom">
                        <div class="col-sm-12 features section-description wow fadeInRight" >
                            <h2>Board of <span> Management</span></h2>
                            <div class="divider-1"><div class="line"></div></div>
                        </div>
                    </div>
                    
                    <div class="row justify-content-md-center wow fadeInLeftBig">
                        @foreach($teams as $team)
                            @if($team->profile_division == "DPI")
                                @if($team->profile_position == "Chief Executive Officer")
                                    <div class="col-12">
                                        <div class="profile-header-container">   
                                            <div class="profile-header-img">
                                                <img class="rounded-circle img-fluid" src="assets/img/teams/{{$team->profile_photo}}" />
                                                <!-- badge -->
                                                <div class="jabatan-label-container">
                                                    <span class="label label-default jabatan-label">Chief Executive Officer</span>
                                                    <h4 class="profile-name">{{$team->profile_name}}</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @elseif($team->profile_position == "Chief Finance Officer")
                                    <div class="col-12 offset-sm-4 col-sm-8">
                                        <div class="profile-header-container">   
                                            <div class="profile-header-img">
                                                <img class="rounded-circle img-fluid" src="assets/img/teams/{{$team->profile_photo}}" />
                                                <!-- badge -->
                                                <div class="jabatan-label-container">
                                                    <span class="label label-default jabatan-label">{{$team->profile_position}}</span>
                                                    <h4 class="profile-name">{{$team->profile_name}}</h4>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                @else
                                    <div class="col-12 col-sm-12 col-md-4">
                                        <div class="profile-header-container"> 
                                            <div class="profile-header-img">
                                                <img class="rounded-circle img-fluid" src="assets/img/teams/{{$team->profile_photo}}" />
                                                <!-- badge -->
                                                <div class="jabatan-label-container">
                                                    <span class="label label-default jabatan-label">{{$team->profile_position}}</span>
                                                    <h4 class="profile-name">{{$team->profile_name}}</h4>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                @endif
                            @endif
                        @endforeach
                    </div>    
                </div>
            </div>

            <!-- Marketing-Header -->
            <div class="section-container text" >
                <div class="container-fluid" >
                    <div class="row" id="WhoAreWe">
                        <div class="col-sm-12 features section-description wow fadeInRight" >
                            <h2>Marketing <span> Division</span></h2>
                            <div class="divider-1"><div class="line"></div></div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- eeo --}}
            <div class="section-container text" >
                <div class="container-fluid" >
                    <div class="row" id="eeo">
                        <div class="col-sm-12 features section-description wow fadeInRight" >
                            <h2>External Event <span>Organizer</span></h2>
                            <div class="divider-1"><div class="line"></div></div>
                        </div>
                    </div>
                    
                    <div class="row justify-content-md-center wow fadeInLeftBig">
                        <div class="row justify-content-md-center">
                            @foreach($teams as $team)
                                @if($team->profile_sub_division == "External Event Organizer")
                                    @if($team->profile_position == "Manager")
                                        <div class="col-12">
                                            <div class="profile-header-container">   
                                                <div class="profile-header-img">
                                                    <img class="rounded-circle img-fluid" src="assets/img/teams/{{$team->profile_photo}}" />
                                                    <!-- badge -->
                                                    <div class="jabatan-label-container">
                                                        <span class="label label-default jabatan-label">{{$team->profile_sub_division}}</span>
                                                        <br/>
                                                        <span class="label label-default jabatan-label">{{$team->profile_position}}</span>
                                                        <h4 class="profile-name">{{$team->profile_name}}</h4>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    @else
                                        <div class="col-12 col-sm-12 col-md-6">
                                            <div class="profile-header-container">
                                                <div class="profile-header-img">
                                                    <img class="rounded-circle img-fluid" src="assets/img/teams/{{$team->profile_photo}}" />
                                                    <!-- badge -->
                                                    <div class="jabatan-label-container">
                                                        <span class="label label-default jabatan-label">{{$team->profile_sub_division}}</span>
                                                        <br/>
                                                        <span class="label label-default jabatan-label">{{$team->profile_position}}</span>
                                                        <h4 class="profile-name">{{$team->profile_name}}</h4>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>            
                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            {{-- pr --}}
            <div class="section-container text" >
                <div class="container-fluid" >
                    <div class="row" id="pr">
                        <div class="col-sm-12 features section-description wow fadeInRight" >
                            <h2>Public <span>Relation</span></h2>
                            <div class="divider-1"><div class="line"></div></div>
                        </div>
                    </div>

                    <div class="row justify-content-md-center wow fadeInLeftBig">
                        <div class="row justify-content-md-center mx-auto">
                            @foreach($teams as $team)
                                @if($team->profile_sub_division == "Public Relation")
                                    @if($team->profile_position == "Manager")
                                        <div class="col-12">
                                            <div class="profile-header-container">   
                                                <div class="profile-header-img">
                                                    <img class="rounded-circle img-fluid" src="assets/img/teams/{{$team->profile_photo}}" />
                                                    <!-- badge -->
                                                    <div class="jabatan-label-container">
                                                        <span class="label label-default jabatan-label">{{$team->profile_sub_division}}</span>
                                                        <br/>
                                                        <span class="label label-default jabatan-label">{{$team->profile_position}}</span>
                                                        <h4 class="profile-name">{{$team->profile_name}}</h4>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    @else
                                        <div class="col-12 col-sm-12 col-md-6">
                                            <div class="profile-header-container">
                                                <div class="profile-header-img">
                                                    <img class="rounded-circle img-fluid" src="assets/img/teams/{{$team->profile_photo}}" />
                                                    <!-- badge -->
                                                    <div class="jabatan-label-container">
                                                        <span class="label label-default jabatan-label">{{$team->profile_sub_division}}</span>
                                                        <br/>
                                                        <span class="label label-default jabatan-label">{{$team->profile_position}}</span>
                                                        <h4 class="profile-name">{{$team->profile_name}}</h4>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>


            <!-- Product -->
            <div class="section-container text" >
                <div class="container-fluid" >
                    <div class="row" id="WhoAreWe">
                        <div class="col-sm-12 features section-description wow fadeInRight" >
                            <h2>Product <span> Division</span></h2>
                            <div class="divider-1"><div class="line"></div></div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- lnt --}}
            <div class="section-container text" >
                <div class="container-fluid" >
                    <div class="row" id="lnt">
                        <div class="col-sm-12 features section-description wow fadeInRight" >
                            <h2>Learning<span> and </span>Training</h2>
                            <div class="divider-1"><div class="line"></div></div>
                        </div>
                    </div>
                    
                    <div class="row justify-content-md-center wow fadeInLeftBig">
                        <div class="row justify-content-md-center">
                            @foreach($teams as $team)
                                @if($team->profile_sub_division == "Learning and Training")
                                    @if($team->profile_position == "Manager")
                                        <div class="col-12">
                                            <div class="profile-header-container">   
                                                <div class="profile-header-img">
                                                    <img class="rounded-circle img-fluid" src="assets/img/teams/{{$team->profile_photo}}" />
                                                    <!-- badge -->
                                                    <div class="jabatan-label-container">
                                                        <span class="label label-default jabatan-label">{{$team->profile_sub_division}}</span>
                                                        <br/>
                                                        <span class="label label-default jabatan-label">{{$team->profile_position}}</span>
                                                        <h4 class="profile-name">{{$team->profile_name}}</h4>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    @else
                                        <div class="col-12 col-sm-12 col-md-6">
                                            <div class="profile-header-container">
                                                <div class="profile-header-img">
                                                    <img class="rounded-circle img-fluid" src="assets/img/teams/{{$team->profile_photo}}" />
                                                    <!-- badge -->
                                                    <div class="jabatan-label-container">
                                                        <span class="label label-default jabatan-label">{{$team->profile_sub_division}}</span>
                                                        <br/>
                                                        <span class="label label-default jabatan-label">{{$team->profile_position}}</span>
                                                        <h4 class="profile-name">{{$team->profile_name}}</h4>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            {{-- fave --}}
            <div class="section-container text" >
                <div class="container-fluid" >
                    <div class="row" id="fave">
                        <div class="col-sm-12 features section-description wow fadeInRight" >
                            <h2>Fave <span> Solution</span></h2>
                            <div class="divider-1"><div class="line"></div></div>
                        </div>
                    </div>
                    
                    <div class="row justify-content-md-center wow fadeInLeftBig">
                        <div class="row justify-content-md-center mx-auto">
                            @foreach($teams as $team)
                                @if($team->profile_sub_division == 'Fave Solution')
                                    @if($team->profile_position == 'Manager')
                                        <div class="col-12">
                                            <div class="profile-header-container">   
                                                <div class="profile-header-img">
                                                    <img class="rounded-circle img-fluid" src="assets/img/teams/{{$team->profile_photo}}" />
                                                    <!-- badge -->
                                                    <div class="jabatan-label-container">
                                                        <span class="label label-default jabatan-label">{{$team->profile_sub_division}}</span>
                                                        <br/>
                                                        <span class="label label-default jabatan-label">{{$team->profile_position}}</span>
                                                        <h4 class="profile-name">{{$team->profile_name}}</h4>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    @else
                                        <div class="col-12 col-sm-12 col-md-6">
                                            <div class="profile-header-container">
                                                <div class="profile-header-img">
                                                    <img class="rounded-circle img-fluid" src="assets/img/teams/{{$team->profile_photo}}" />
                                                    <!-- badge -->
                                                    <div class="jabatan-label-container">
                                                        <span class="label label-default jabatan-label">{{$team->profile_sub_division}}</span>
                                                        <br/>
                                                        <span class="label label-default jabatan-label">{{$team->profile_position}}</span>
                                                        <h4 class="profile-name">{{$team->profile_name}}</h4>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div> 
                </div>
            </div>

            {{-- file --}}
            <div class="section-container text" >
                <div class="container-fluid" >
                    <div class="row" id="file">
                        <div class="col-sm-12 features section-description wow fadeInRight" >
                            <h2>FILE <span> Magz</span></h2>
                            <div class="divider-1"><div class="line"></div></div>
                        </div>
                    </div>
                    
                    <div class="row justify-content-md-center wow fadeInLeftBig">
                        <div class="row justify-content-md-center">
                            @foreach($teams as $team)
                                @if($team->profile_sub_division == "File Magz")
                                    @if($team->profile_position == "Manager")
                                        <div class="col-12">
                                            <div class="profile-header-container">   
                                                <div class="profile-header-img">
                                                    <img class="rounded-circle img-fluid" src="assets/img/teams/{{$team->profile_photo}}" />
                                                    <!-- badge -->
                                                    <div class="jabatan-label-container">
                                                        <span class="label label-default jabatan-label">{{$team->profile_sub_division}}</span>
                                                        <br/>
                                                        <span class="label label-default jabatan-label">{{$team->profile_position}}</span>
                                                        <h4 class="profile-name">{{$team->profile_name}}</h4>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    @else
                                        <div class="col-12 col-sm-12 col-md-6">
                                            <div class="profile-header-container">
                                                <div class="profile-header-img">
                                                    <img class="rounded-circle img-fluid" src="assets/img/teams/{{$team->profile_photo}}" />
                                                    <!-- badge -->
                                                    <div class="jabatan-label-container">
                                                        <span class="label label-default jabatan-label">{{$team->profile_sub_division}}</span>
                                                        <br/>
                                                        <span class="label label-default jabatan-label">{{$team->profile_position}}</span>
                                                        <h4 class="profile-name">{{$team->profile_name}}</h4>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>            
                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            <!-- Internal -->
            <div class="section-container text" >
                <div class="container-fluid" >
                    <div class="row" id="WhoAreWe">
                        <div class="col-sm-12 features section-description wow fadeInRight" >
                            <h2>Internal <span> Division</span></h2>
                            <div class="divider-1"><div class="line"></div></div>
                        </div>
                    </div>   
                </div>
            </div>

            {{-- rnd --}}
            <div class="section-container text" >
                <div class="container-fluid" >
                    <div class="row" id="rnd">
                        <div class="col-sm-12 features section-description wow fadeInRight" >
                            <h2>Research <span>And</span> Development</h2>
                            <div class="divider-1"><div class="line"></div></div>
                        </div>
                    </div>
                    <div class="row justify-content-md-center wow fadeInLeftBig">
                        <div class="row justify-content-md-center">
                            @foreach($teams as $team)
                                @if($team->profile_sub_division == 'Research And Development')
                                    @if($team->profile_position == 'Manager')
                                        <div class="col-12">
                                            <div class="profile-header-container">   
                                                <div class="profile-header-img">
                                                    <img class="rounded-circle img-fluid" src="assets/img/teams/{{$team->profile_photo}}" />
                                                    <!-- badge -->
                                                    <div class="jabatan-label-container">
                                                        <span class="label label-default jabatan-label">{{$team->profile_sub_division}}</span>
                                                        <br/>
                                                        <span class="label label-default jabatan-label">{{$team->profile_position}}</span>
                                                        <h4 class="profile-name">{{$team->profile_name}}</h4>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    @else
                                        <div class="col-12 col-sm-12 col-md-4">
                                            <div class="profile-header-container">
                                                <div class="profile-header-img">
                                                    <img class="rounded-circle img-fluid" src="assets/img/teams/{{$team->profile_photo}}" />
                                                    <!-- badge -->
                                                    <div class="jabatan-label-container">
                                                        <span class="label label-default jabatan-label">{{$team->profile_sub_division}}</span>
                                                        <br/>
                                                        <span class="label label-default jabatan-label">{{$team->profile_position}}</span>
                                                        <h4 class="profile-name">{{$team->profile_name}}</h4>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>    
                </div>
            </div>

            {{-- hrd --}}
            <div class="section-container text" >
                <div class="container-fluid" >
                    <div class="row" id="hrd">
                        <div class="col-sm-12 features section-description wow fadeInRight" >
                            <h2>Human<span> Resources </span>Development</h2>
                            <div class="divider-1"><div class="line"></div></div>
                        </div>
                    </div>
                    
                    <div class="row justify-content-md-center wow fadeInLeftBig">
                        <div class="row justify-content-md-center">
                            @foreach($teams as $team)
                                @if($team->profile_sub_division == "Human Resource Development")
                                    @if($team->profile_position == "Manager")
                                        <div class="col-12">
                                            <div class="profile-header-container">   
                                                <div class="profile-header-img">
                                                    <img class="rounded-circle img-fluid" src="assets/img/teams/{{$team->profile_photo}}" />
                                                    <!-- badge -->
                                                    <div class="jabatan-label-container">
                                                        <span class="label label-default jabatan-label">{{$team->profile_sub_division}}</span>
                                                        <br/>
                                                        <span class="label label-default jabatan-label">{{$team->profile_position}}</span>
                                                        <h4 class="profile-name">{{$team->profile_name}}</h4>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    @else
                                        <div class="col-12 col-sm-12 col-md-6">
                                            <div class="profile-header-container">
                                                <div class="profile-header-img">
                                                    <img class="rounded-circle img-fluid" src="assets/img/teams/{{$team->profile_photo}}" />
                                                    <!-- badge -->
                                                    <div class="jabatan-label-container">
                                                        <span class="label label-default jabatan-label">{{$team->profile_sub_division}}</span>
                                                        <br/>
                                                        <span class="label label-default jabatan-label">{{$team->profile_position}}</span>
                                                        <h4 class="profile-name">{{$team->profile_name}}</h4>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>            
                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-sm-6 footer-copyright float-left text-left">
                            
                            &copy; 2018 <a href="https://bncc.net">Bina Nusantara Computer Club</a>, All Right Reserved.
                            <br/>
                            Create with &#x2764; by Research and Development BNCC 29th
                        </div>
                        <div class="col-12 col-sm-6 float-right text-right align-self-center">
                                <a class="px-1" target="_blank" href="https://www.instagram.com/bnccbinus/"><i class="fab fa-instagram fa-2x"></i></a>
                                <a class="px-1" target="_blank" href="https://www.facebook.com/bina.nusantara.computer.club/"><i class="fab fa-facebook fa-2x"></i></a>
                                <a class="px-1" target="_blank" href="https://twitter.com/bncc_binus"><i class="fab fa-twitter fa-2x"></i></a>
                            </div>
                    </div>
                </div>
            </footer>

            <!-- Optional JavaScript -->
            <!-- <script src="assets/js/jquery-3.3.1.min.js"></script> -->
            <script src="assets/js/jquery-1.11.1.min.js"></script>
            <script src="assets/js/bootstrap.bundle.min.js"></script>
            <script src="assets/js/jquery.backstretch.min.js"></script>
            <script src="assets/js/wow.min.js"></script>
            <script src="assets/js/waypoints.min.js"></script>
            <!-- <script src="assets/slick/slick.min.js"></script> -->
            <script src="assets/animsition/js/animsition.min.js"></script>
            <script src="assets/js/custom.js"></script>
            <script src="assets/js/organization.js"></script>
            <!-- <script src="assets/js/timeline.js"></script> -->
        </body>
    </div>
</html>