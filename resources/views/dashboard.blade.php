@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard <a href="{{route('user.edit', ['user'=>Auth::user()->id])}}" class="btn btn-primary">Edit Profile</a></h1>
@stop

@section('content')
    <p>Welcome {{strtoupper(Auth::user()->role)}}.</p>
	@if(session('message'))
		<div class="alert alert-danger">
			{{session('message')}}
		</div>
	@endif
@stop

{{-- @section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop --}}