<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
            'email' => 'rnd@bncc.net',
            'password' => bcrypt('secret'),
            'role' => 'root',
        ]);

        DB::table('users')->insert([
            'email' => 'dpi@bncc.net',
            'password' => bcrypt('secret'),
            'role' => 'dpi',
        ]);

        DB::table('users')->insert([
            'email' => 'eeo@bncc.net',
            'password' => bcrypt('secret'),
            'role' => 'eeo',
        ]);

        DB::table('users')->insert([
            'email' => 'pr@bncc.net',
            'password' => bcrypt('secret'),
            'role' => 'pr',
        ]);
    }
}
