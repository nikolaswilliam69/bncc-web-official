function pageReady(){
    $(window).load(function(){
        // Dipakai kalau gak pakai animsition js (preloader manual)
        // setTimeout(() => {
        //     $('#preloader').fadeOut('slow',function(){$(this).remove();});
        // }, 500);
        
        
        /*
            Wow JS Load
            So Much Wow !
            ditaruh dalem setTimeout kalau mau pake preloader manual
        */
        new WOW().init();
    });

}

// Smooth Scrolling
    $('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .not('[href*="carousel"][href*="Event"]') // agar navigation carousel tidak scrolling
    .click(function(event) {
        // On-page links
        if (
        location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
        && 
        location.hostname == this.hostname
        ) {
        // Figure out element to scroll to
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        // Does a scroll target exist?
        if (target.length) {
            // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000, function() {
            // Callback after animation
            // Must change focus!
            var $target = $(target);
            $target.focus();
            if ($target.is(":focus")) { // Checking if the target was focused
                return false;
            } else {
                $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                $target.focus(); // Set focus again
            };
            });
        }
        }
    });

$(document).ready(function() {
    $(".animsition").animsition({
        //   inClass: 'fade-in',
      inClass: 'fade-in-left',
      outClass: 'fade-out-right',
      inDuration: 1000,
      outDuration: 800,
        //   linkElement: '.animsition-link',
      linkElement: 'a:not([target="_blank"]):not([href^=#])',
      loading: true,
      loadingParentElement: 'body', //animsition wrapper element
      loadingClass: 'animsition-loading-custom',
      unSupportCss: [
        'animation-duration',
        '-webkit-animation-duration',
        '-o-animation-duration'
      ],
        //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
        //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
      overlay : false,
      overlayClass : 'animsition-overlay-slide',
      overlayParentElement : 'body'
    });
});