jQuery(document).ready(function() {
    /* Navigation */
    // toggle "navbar-no-bg" class
    $('.top-content .text').waypoint(function() {
        $('nav').toggleClass('navbar-no-bg');
        $('nav').toggleClass('navbar-dark');
        $('nav').toggleClass('navbar-light');
        $('div .dropdown-menu').toggleClass('navbar-no-bg');
    });
    /* Background slideshow */
    // $('.top-content').backstretch("assets/img/aktivis29.jpg");
    $('.top-content').backstretch([
        "assets/img/aktivis29.jpg", "assets/img/parralax-2.jpg", "assets/img/parralax-4.jpg", "assets/img/parralax-1.png", "assets/img/parralax-3.jpg", "assets/img/parralax-6.jpg"
    ], {duration: 3000, fade: 750});

    $('#top-navbar-1').on('shown.bs.collapse', function(){
        $('.top-content').backstretch("resize");
    });
    $('#top-navbar-1').on('hidden.bs.collapse', function(){
        $('.top-content').backstretch("resize");
    });
});

// Mission Text Detail
function change1() {
    $("#missionTextDetail").css('opacity', '0.0');
    $("#iconDetail").css('opacity','0.0');
    setTimeout(() => {
        $("#missionTextDetail").text("Creating an organization strongly embraced by culture").css('opacity', '1.0');
            $("#iconDetail").attr("src","assets/svg/mission1.svg").css("opacity","1");
    }, 250);
    
}
  
function change2() {
    $("#missionTextDetail").css('opacity', '0.0');
    $("#iconDetail").css('opacity','0.0');
    setTimeout(() => {
        $("#missionTextDetail").text("Providing relevant computer education to Binus students").css('opacity', '1.0');
            $("#iconDetail").attr("src","assets/svg/mission2.svg").css("opacity","1");
    }, 250);
}

function change3() {
    $("#missionTextDetail").css('opacity', '0.0');
    $("#iconDetail").css('opacity','0.0');
    setTimeout(() => {
        $("#missionTextDetail").text("Building passionate communities of technology trends").css('opacity', '1.0');
        $("#iconDetail").attr("src","assets/svg/mission3.svg").css("opacity","1");
    }, 250);
}

function change4() {
    $("#missionTextDetail").css('opacity', '0.0');
    $("#iconDetail").css('opacity','0.0');
    setTimeout(() => {
        $("#missionTextDetail").text("Developing reliable technology products and services").css('opacity', '1.0');
         $("#iconDetail").attr("src","assets/svg/mission4.svg").css("opacity","1");
        $("#iconDetail")
    }, 250);
}

function change5() {
    $("#missionTextDetail").css('opacity', '0.0');
    $("#iconDetail").css('opacity','0.0');
    setTimeout(() => {
        $("#missionTextDetail").text("Empower the organization's relation to the professional world").css('opacity', '1.0');
            $("#iconDetail").attr("src","assets/svg/mission5.svg").css("opacity","1");
    }, 250);
}

function change6() {
    $("#missionTextDetail").css('opacity', '0.0');
    $("#iconDetail").css('opacity','0.0');
    setTimeout(() => {
        $("#missionTextDetail").text("Giving social contribution to the people of Indonesia").css('opacity', '1.0');
            $("#iconDetail").attr("src","assets/svg/mission6.svg").css("opacity","1");
    }, 250);
}



$('a').click(function(e) {
    var value = $(this).attr('href');

    if ($(this).is('.button-product')) {
        var productId = $(this).attr('productid');

        if (productId == $('.product-desc').parent().attr('curr-id')) return;

        $('.product-desc').parent().attr('curr-id', productId);
        
        if ($('.product-desc.show').length > 0) {
            $('.product-desc.show').animate({
                opacity: 0
            },400,"swing", function () {
                $(this).removeClass('show');
                $('.product-desc[productid='+productId+']')
                    .addClass('show').animate({'opacity' : 1});
            });
        }
        else {
            $('.product-desc[productid='+productId+']')
                .addClass('show').animate({'opacity' : 1});
        }
    }
    
    //maaf barbar
    // if (value == '#fave' || value == '#file' || value == '#lnt') {
    //     $(".product-desc").css('min-height', '250px');
    //     setTimeout(() => {
    //         $("#product-desc-div").css('display','block').css('opacity','0');
    //         $("#product-desc-url").css('display','block').css('opacity','0');
    //         if(value == '#fave') { 
    //             e.preventDefault(); //preventing the link from being followed.
    //             setTimeout(() => {
    //                 $("#product-desc-div").css('opacity','1');
    //                 $("#product-desc-div").find('h4').html('<span>Fave Solution</span> is software house which do website and mobile application development as an IT solution for personal and companies').attr("class","fave");
    //                 $("#product-desc-url").css('opacity','1');
    //                 $("#product-desc-url").find('a').attr("href", "https://favesolution.com/");
    //                 $("#product-desc-url").find('a').html('<i class="fas fa-globe"></i> Visit Fave Solution');
    //             }, 250);  
    //          } else if(value == '#file') { 
    //             e.preventDefault(); //preventing the link from being followed.
    //             setTimeout(() => {
    //                 $("#product-desc-div").css('opacity','1');
    //                 $("#product-desc-div").find('h4').html('<span>FILE Magz</span>, an online media that provides informative, educative, and up-to-date information regarding trends of technology, business, and youth lifestyle.').attr("class","file");
    //                 $("#product-desc-url").css('opacity','1');
    //                 $("#product-desc-url").find('a').attr("href", "https://www.filemagz.com/");
    //                 $("#product-desc-url").find('a').html('<i class="fas fa-globe"></i> Visit File Magazine');
    //             }, 250);
    //          } else if(value == '#lnt') { 
    //             e.preventDefault(); //preventing the link from being followed.
    //             setTimeout(() => {
    //                 $("#product-desc-div").css('opacity','1');
    //                 $("#product-desc-div").find('h4').html('<span>BNCC Learning and Training (LnT)</span> provides members with various IT courses that relates with the industry. Members can then choose to learn one of the available courses. The end goal of LnT is to provide members with skill and knowledge that can be used in their proffesional life in the future.').attr("class","lnt");
    //                 $("#product-desc-url").css('opacity','1');
    //                 $("#product-desc-url").find('a').attr("href", "https://member.bncc.net/");
    //                 $("#product-desc-url").find('a').html('<i class="fas fa-globe"></i> Visit BNCC LnT');
    //             }, 250);
    //          }
    //     }, 350);
    // }
    
});



$(document).ready(function() {
    //Set the carousel options
    $('.carousel').carousel({
      pause: true,
      interval: 4000,
    });
  });



  var prevButton = '<button type="button" data-role="none" class="slick-prev" aria-label="prev"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" version="1.1"><path fill="#FFFFFF" d="M 16,16.46 11.415,11.875 16,7.29 14.585,5.875 l -6,6 6,6 z" /></svg></button>',
    nextButton = '<button type="button" data-role="none" class="slick-next" aria-label="next"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path fill="#FFFFFF" d="M8.585 16.46l4.585-4.585-4.585-4.585 1.415-1.415 6 6-6 6z"></path></svg></button>';

$('.slider-testimonial').slick({
  infinite: true,
  dots: true,
  autoplay: true,
  autoplaySpeed: 4000,
  speed: 1000,
  cssEase: 'ease-in-out',
  prevArrow: prevButton,
  nextArrow: nextButton
});

$('.quote-container').mousedown(function(){
  $('.single-item').addClass('dragging');
});
$('.quote-container').mouseup(function(){
  $('.single-item').removeClass('dragging');
});